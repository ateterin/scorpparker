﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace pnfw1
{
    public partial class Login : System.Web.UI.Page
    {
        string strUser = "";
        string strPassword = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox1.Focus();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            strUser = TextBox1.Text;
            strPassword = TextBox2.Text;

            dsLoginUsers.dtLoginUsersDataTable dtUsers = new dsLoginUsers.dtLoginUsersDataTable();
            dsLoginUsersTableAdapters.dtLoginUsersTableAdapter daUsers = new dsLoginUsersTableAdapters.dtLoginUsersTableAdapter();
            dsSecurity.dtSecurityDataTable dtSecurity = new dsSecurity.dtSecurityDataTable();
            dsSecurityTableAdapters.dtSecurityTableAdapter daSecurity = new dsSecurityTableAdapters.dtSecurityTableAdapter();

            Session["RefSC"] = "S";

            if (strUser.Length > 2)
            {
                daUsers.Fill(dtUsers, strUser, strPassword);
                daSecurity.Fill(dtSecurity, strUser);

                if (dtUsers.Rows.Count > 0)
                {
                    if (dtSecurity.Rows.Count > 0)
                    {
                        Session["UserID"] = dtUsers.Rows[0].ItemArray[0].ToString();
                        Session["UserName"] = dtUsers.Rows[0].ItemArray[1].ToString() + " " + dtUsers.Rows[0].ItemArray[2].ToString();
                        Session["UserLogin"] = strUser;
                        Session["Locations"] = dtSecurity.Rows[0].ItemArray[4].ToString();
                        Session["Level"] =  dtSecurity.Rows[0].ItemArray[5].ToString();

                        Session["indA"] = "1";

                        PW.dtReqRates.Clear();
                        DataRow dr = PW.dtReqRates.NewRow();

                        dr[0] = 1;
                        dr[1] = 100;
                        dr[2] = "Web";
                        //dr[3] = DateTime.Now.ToString("M/d/yyyy");
                        dr[3] = Convert.ToDateTime(DateTime.Now.ToString("M/d/yyyy"), System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                        //dr[4] = DateTime.Now.ToString("M/d/yyyy");
                        dr[4] = Convert.ToDateTime(DateTime.Now.ToString("M/d/yyyy"), System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                        dr[5] = "DOLL";
                        dr[6] = "3189";
                        dr[7] = "Mo,Tu,We,Th,Fr,Sa,Su";
                        dr[8] = "Mo,Tu,We,Th,Fr,Sa,Su";
                        dr[9] = 1;
                        dr[10] = 365;
                        dr[11] = 0;
                        dr[12] = 0;
                        dr[13] = 0;
                        dr[14] = 0;
                        dr[17] = Session["UserName"];
                        dr[18] = DateTime.Now;

                        PW.dtReqRates.Rows.Add(dr);

                        string connectionString = ConfigurationManager.ConnectionStrings["PARKER_RPTConnectionString"].ConnectionString;

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            SqlCommand cmd = new SqlCommand();

                            cmd.CommandText = "INSERT INTO web_activity (user_name,dt) VALUES ('" + Session["UserName"] + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")  + "')";
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = connection;

                            connection.Open();
                            cmd.ExecuteNonQuery();
                        }


                        Response.Redirect("Navigator.aspx");
                        Label4.Visible = false;
                    }
                    else
                    {
                        Label4.Visible = true;
                    }
                }
                else
                {
                    Label4.Visible = true;
                }
            }
            else
            {
                Label4.Visible = true;
            }
        }
    }
}