﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pnfw1
{
    public partial class Navigator : System.Web.UI.Page
    {

        dsUserApp.dtUserAppAllowDataTable dtUAA = new dsUserApp.dtUserAppAllowDataTable();
        dsUserAppTableAdapters.dtUserAppAllowTableAdapter daUAA = new dsUserAppTableAdapters.dtUserAppAllowTableAdapter();


        protected void Page_Load(object sender, EventArgs e)
        {

            string x = Session["UserName"].ToString();
            GData.strUserName = x;

            Button6.Visible = false;

            //if (Session["Locations"].ToString().IndexOf("100") == -1)
            if ((Convert.ToInt32(Session["Level"]) < 3 && Session["Locations"].ToString().Contains("100")) || (Convert.ToInt32(Session["Level"]) == 7 && Session["Locations"].ToString().Contains("100")))
            {
                Button6.Visible = true;
            }

            daUAA.Fill(dtUAA, Convert.ToInt32(Session["UserID"]), "WEB_REFERRALS_SEARCH");

            if (dtUAA.Rows.Count > 0)
            {
                Button7.Visible = true;
            }
            else
            {
                Button7.Visible = false;
            }

            daUAA.Fill(dtUAA, Convert.ToInt32(Session["UserID"]), "WEB_REFERRALS_UPDATE");
            if (dtUAA.Rows.Count > 0)
            {
                Session["RUpdate"] = "Y";
            }
            else
            {
                Session["RUpdate"] = "N";
            }

            daUAA.Fill(dtUAA, Convert.ToInt32(Session["UserID"]), "WEB_REFERRALS_CREATE_COUPON_GROUPS");
            if (dtUAA.Rows.Count > 0)
            {
                Session["CCreate"] = "Y";
            }
            else
            {
                Session["CCreate"] = "N";
            }




        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewRates.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
          
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Response.Redirect("Charts.aspx");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Response.Redirect("RateSwitch.aspx");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("JockeyLabel.aspx");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Session["Expression"] = "referral_id";
            Session["Direction"] = " ASC";
            Response.Redirect("Referrals.aspx");

        }
    }
}