﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace pnfw1
{
    public partial class RateSwitch : System.Web.UI.Page
    {
        string strSet = "";
        DataTable dtDates = new DataTable();

        dsDRatesSets.drates_setsDataTable dtDRatesSets = new dsDRatesSets.drates_setsDataTable();
        dsDRatesSetsTableAdapters.drates_setsTableAdapter daDRatesSets = new dsDRatesSetsTableAdapters.drates_setsTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["indA"].ToString() != "1")
                {
                    string close = @"<script type='text/javascript'>
                                window.returnValue = true; 
                                window.close();
                                </script>";
                    base.Response.Write(close);
                }

                //daDRatesSets.Fill(dtDRatesSets, 2);

                //DropDownList1.DataSource = dtDRatesSets;
                //DropDownList1.DataTextField = "description";
                //DropDownList1.DataValueField = "set_id";
                //DropDownList1.DataBind();

                Label2.Visible = false;

                DropDownList1.Items.Add(new ListItem("Please select locations", "0"));
                DropDownList1.SelectedValue = "0";

                string strLocations = (string)Session["Locations"];
                string strAllLocations = ConfigurationManager.AppSettings["AllLocations"];

                //if (strLocations == "100/101/102" || strLocations == "100/101/102/103/104/105/106/107/108/113/129")
                if (strLocations == "100/101/102" || strLocations == strAllLocations)

                {
                    DropDownList1.Items.Add(new ListItem("SelfPark/Economy", "3"));
                    strSet = "3";
                }

                if (strLocations == "105/106/107" || strLocations == strAllLocations || strLocations == "105/106/107/129/130")
                {
                    DropDownList1.Items.Add(new ListItem("ExpressA/ExpressB", "4"));
                    strSet = "4";
                }

                if (DropDownList1.Items.Count == 2)
                {
                    ListItem Temp = DropDownList1.Items[0];
                    DropDownList1.Items.Remove(Temp);                   
                }

                dtDates.Clear();

                if (DropDownList1.Items.Count == 1)
                {
                    CalendarDataUpdate();
 
                    Calendar1.Visible = true;
                }
                else
                {
                    Calendar1.Visible = false;
                }

                if (Convert.ToInt32(Session["Level"].ToString()) > 2)
                {
                    Calendar1.Visible = false;
                }
            }

            Calendar1.SelectedDates.Clear();

            if (Convert.ToInt32(Session["Level"].ToString()) > 2)
            {
                Calendar1.Visible = false;
            }
            
        }

        protected void CalendarRenderer(object sender, DayRenderEventArgs e)
        {
            foreach (DataRow row in dtDates.Rows)
            {
                string strdt = row.ItemArray[0].ToString();
                string strvld = row.ItemArray[1].ToString();
                DateTime dte = Convert.ToDateTime(strdt);

                if (e.Day.Date == dte && strvld == "Y")
                {
                    e.Cell.BackColor = System.Drawing.Color.DarkGreen;
                    e.Cell.ForeColor = System.Drawing.Color.Black;
                }
                if (e.Day.Date == dte && strvld == "N")
                {
                    e.Cell.BackColor = System.Drawing.Color.LightGreen;
                    e.Cell.ForeColor = System.Drawing.Color.Black;
                }
                if (e.Day.IsToday)
                {
                    e.Cell.BorderStyle = BorderStyle.Solid;
                    e.Cell.BorderColor = System.Drawing.Color.Yellow;
                }
                else
                {
                    e.Cell.BorderStyle = BorderStyle.None;
                }
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (DropDownList1.SelectedValue == "3" || DropDownList1.SelectedValue == "4")
            {
                DropDownList1.Items.Remove(DropDownList1.Items.FindByValue("0"));
                if (Convert.ToInt32(Session["Level"].ToString()) > 2)
                {
                    Calendar1.Visible = false;
                }
                else
                {
                    Calendar1.Visible = true;
                }
            }
            else
            {
                Calendar1.Visible = false;
            }

            CalendarDataUpdate();
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            Label2.Visible = false;

            CalendarDataUpdate();
            
            DateTime dateSelected = Calendar1.SelectedDate;

            var vld = (from row in dtDates.AsEnumerable()
                       where row.Field<DateTime>("sdate") == dateSelected
                       select row["valid"]).ToList();

            string svld = "";

            if (vld.Count > 0)
            {
                svld = (string)vld[0];
            }
            else
            {
                svld = "E";
            }

            string sql = "";

            switch (svld)
            {
                case "Y":
                    sql = "UPDATE drates_switch_dates SET valid = 'N' WHERE sdate = '" + dateSelected + "' AND set_id = '" + strSet + "'";
                    break;
                case "N":
                    sql = "UPDATE drates_switch_dates SET valid = 'Y' WHERE sdate = '" + dateSelected + "' AND set_id = '" + strSet + "'";
                    break;
                default:
                    sql = "INSERT INTO drates_switch_dates(sdate,set_id,valid) VALUES('" + dateSelected + "','" + strSet + "','Y')";
                    break;
            }

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DYNAMIC_RATESConnectionString"].ToString());
            try
            {
                con.Open();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Label2.Text = "ERROR! " + ex.ToString();
                Label2.Visible = true;
            }
            finally
            {
                con.Close();
            }

            CalendarDataUpdate();

            if (Calendar1.SelectedDate.Year == DateTime.Today.Year && Calendar1.SelectedDate.Month == DateTime.Today.Month && Calendar1.SelectedDate.Day == DateTime.Today.Day)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("p_drates_switch", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SMode", SqlDbType.VarChar).Value = "LIVE";
                    cmd.Parameters.Add("@SSetId", SqlDbType.BigInt).Value = Convert.ToInt64(strSet);
                    cmd.Parameters.Add("@SResult", SqlDbType.Int);
                    cmd.Parameters["@SResult"].Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    //Label2.Text =  "Result is: " + cmd.Parameters["@SResult"].Value.ToString();
                }
                catch (Exception ex)
                {
                    Label2.Text = "ERROR! " + ex.ToString();
                    Label2.Visible = true;
                }
                finally
                {
                    con.Close();
                }
                
            }

        }

        protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
           Label2.Visible = false; ;
           CalendarDataUpdate();
        }

        protected void CalendarDataUpdate()
        {
            strSet = DropDownList1.SelectedValue.ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DYNAMIC_RATESConnectionString"].ToString());
            dtDates.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                string strSQL = "select cast(sdate as DATE) as sdate, valid from drates_switch_dates where set_id = '" + strSet + "' ";
                cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                dtDates = ds.Tables[0];
                Label2.Visible = false;
            }
            catch (Exception ex)
            {
                Label2.Text = "ERROR! " + ex.ToString();
                Label2.Visible = true;
            }
            finally
            {
                con.Close();
            }
        }
    }
}