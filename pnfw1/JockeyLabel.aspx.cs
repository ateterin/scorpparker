﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.ServiceProcess;

namespace pnfw1
{
    public partial class JockeyLabel : System.Web.UI.Page
    {

        //ServiceController sc = new ServiceController("JockPrnMgrService");


        protected void JockeyPrinterServiceStatus()
        {
            ServiceController scj = new ServiceController("JockPrnMgrService");

            switch (scj.Status)
            {
                case ServiceControllerStatus.Running:
                    Label2.Text = "Jockey Printer is running";
                    Label2.ForeColor = System.Drawing.Color.DarkGreen;
                    Button2.Enabled = true;
                    Button2.ForeColor = System.Drawing.Color.DarkGreen;
                    Button1.Enabled = false;
                    Button1.ForeColor = System.Drawing.Color.LightGreen;
                    break;
                case ServiceControllerStatus.Stopped:
                    Label2.Text = "Jockey Printer is NOT running";
                    Label2.ForeColor = System.Drawing.Color.Red;
                    Button1.Enabled = true;
                    Button1.ForeColor = System.Drawing.Color.DarkGreen;
                    Button2.Enabled = false;
                    Button2.ForeColor = System.Drawing.Color.LightGreen;
                    break;
                case ServiceControllerStatus.Paused:
                    Label2.Text = "Jockey Printer is NOT running";
                    Label2.ForeColor = System.Drawing.Color.Red;
                    Button1.Enabled = true;
                    Button1.ForeColor = System.Drawing.Color.DarkGreen;
                    Button2.Enabled = false;
                    Button2.ForeColor = System.Drawing.Color.LightGreen;

                    break;
                //case ServiceControllerStatus.StopPending:
                //    return "Stopping";
                //case ServiceControllerStatus.StartPending:
                //    return "Starting";
                default:
                    Label2.Text = "Status Changing, please click on Navigator";
                    Label2.ForeColor = System.Drawing.Color.Red;
                    //Button1.Enabled = true;
                    //Button1.ForeColor = System.Drawing.Color.DarkGreen;
                    //Button2.Enabled = false;
                    //Button2.ForeColor = System.Drawing.Color.LightGreen;

                    break;
            }  
        }

        protected void LabelPrinterServiceStatus()
        {
            ServiceController scl = new ServiceController("LblPrnMgrService");

            switch (scl.Status)
            {
                case ServiceControllerStatus.Running:
                    Label3.Text = "Label Printer is running";
                    Label3.ForeColor = System.Drawing.Color.DarkGreen;
                    Button4.Enabled = true;
                    Button4.ForeColor = System.Drawing.Color.DarkGreen;
                    Button3.Enabled = false;
                    Button3.ForeColor = System.Drawing.Color.LightGreen;
                    break;
                case ServiceControllerStatus.Stopped:
                    Label3.Text = "Label Printer is NOT running";
                    Label3.ForeColor = System.Drawing.Color.Red;
                    Button3.Enabled = true;
                    Button3.ForeColor = System.Drawing.Color.DarkGreen;
                    Button4.Enabled = false;
                    Button4.ForeColor = System.Drawing.Color.LightGreen;
                    break;
                case ServiceControllerStatus.Paused:
                    Label3.Text = "Label Printer is NOT running";
                    Label3.ForeColor = System.Drawing.Color.Red;
                    Button3.Enabled = true;
                    Button3.ForeColor = System.Drawing.Color.DarkGreen;
                    Button4.Enabled = false;
                    Button4.ForeColor = System.Drawing.Color.LightGreen;

                    break;
                //case ServiceControllerStatus.StopPending:
                //    return "Stopping";
                //case ServiceControllerStatus.StartPending:
                //    return "Starting";
                default:
                    Label3.Text = "Status Changing, please click on Navigator";
                    Label3.ForeColor = System.Drawing.Color.Red;
                    //Button1.Enabled = true;
                    //Button1.ForeColor = System.Drawing.Color.DarkGreen;
                    //Button2.Enabled = false;
                    //Button2.ForeColor = System.Drawing.Color.LightGreen;

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            JockeyPrinterServiceStatus();
            LabelPrinterServiceStatus();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ServiceController scj1 = new ServiceController("JockPrnMgrService");

            if (scj1 != null)
            {
                try
                {
                  
                    //sc.Stop();
                    //sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(50));
                    scj1.Start();
                }
                catch (Exception ex)
                {
                    //Todo: throw new application exeception
                }

                System.Threading.Thread.Sleep(500);

                JockeyPrinterServiceStatus();
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            ServiceController scj2 = new ServiceController("JockPrnMgrService");

            if (scj2 != null)
            {
                try
                {
                    scj2.Stop();
                    scj2.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(50));
                }
                catch (Exception ex)
                {
                    //Todo: throw new application exeception
                }

                System.Threading.Thread.Sleep(500);

                JockeyPrinterServiceStatus();
            }

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ServiceController scl1 = new ServiceController("LblPrnMgrService");

            if (scl1 != null)
            {
                try
                {

                    //sc.Stop();
                    //sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(50));
                    scl1.Start();
                }
                catch (Exception ex)
                {
                    //Todo: throw new application exeception
                }

                System.Threading.Thread.Sleep(500);

                LabelPrinterServiceStatus();
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            ServiceController scl2 = new ServiceController("LblPrnMgrService");

            if (scl2 != null)
            {
                try
                {
                    scl2.Stop();
                    scl2.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(50));
                }
                catch (Exception ex)
                {
                    //Todo: throw new application exeception
                }

                System.Threading.Thread.Sleep(500);

                LabelPrinterServiceStatus();
            }
 
        }
    }
}