﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Gma.QrCodeNet.Encoding.Windows.Render;
using Gma.QrCodeNet.Encoding;


namespace pnfw1
{
    public partial class NewRates : System.Web.UI.Page
    {
        dsLocations.locationsDataTable dtLocations = new dsLocations.locationsDataTable();
        dsLocationsTableAdapters.locationsTableAdapter daLocations = new dsLocationsTableAdapters.locationsTableAdapter();

        dsRackRate.dtRackRateDataTable dtRR = new dsRackRate.dtRackRateDataTable();
        dsRackRateTableAdapters.dtRackRateTableAdapter daRR = new dsRackRateTableAdapters.dtRackRateTableAdapter();

        //dsRateRequests.dtRateRequestsDataTable dtReqRates = new dsRateRequests.dtRateRequestsDataTable();

        DateTime sdate;

        //BarcodeLib.Barcode bar = new BarcodeLib.Barcode();
        ////System.Drawing.Bitmap b;
        //System.Drawing.Image b;

        private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert,
                                       System.Drawing.Imaging.ImageFormat formatOfImage)
        {
            byte[] Ret;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    imageToConvert.Save(ms, formatOfImage);
                    Ret = ms.ToArray();
                }
            }
            catch (Exception) { throw; }
            return Ret;
        }


        protected void Page_Load(object sender, EventArgs e)
        {


           


            if (Session["indA"].ToString() != "1")
            {

                string close = @"<script type='text/javascript'>
                                window.returnValue = true; 
                                window.close();
                                </script>";
                base.Response.Write(close);

            }

            if (!IsPostBack)
            {
                PW.indB = 0;

                sdate = DateTime.Now;

                daLocations.Fill(dtLocations, (string)Session["Locations"]);
                DropDownList1.DataSource = dtLocations;
                DropDownList1.DataTextField = "location";
                DropDownList1.DataValueField = "location_id";
                DropDownList1.DataBind();
                //ListItem l = new ListItem("Halifax", "113", true);
                //DropDownList1.Items.Add(l);

                PW.location_id = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                PW.Reports = "1";
                ////DropDownList1.Items.FindByValue("Toronto Valet").Selected = true;
                daRR.Fill(dtRR, TextBox1.Text, PW.location_id, sdate);
                Label19.Text = dtRR.Rows[0].ItemArray[3].ToString();
                Label20.Text = dtRR.Rows[0].ItemArray[4].ToString();
                Label21.Text = dtRR.Rows[0].ItemArray[5].ToString();
                Label22.Text = dtRR.Rows[0].ItemArray[6].ToString();
                TextBox6.Text = dtRR.Rows[0].ItemArray[3].ToString();
                TextBox7.Text = dtRR.Rows[0].ItemArray[4].ToString();
                TextBox8.Text = dtRR.Rows[0].ItemArray[5].ToString();
                TextBox9.Text = dtRR.Rows[0].ItemArray[6].ToString();

                TextBox1.Text = "3189";
                CheckBox1.Checked = true;
                CheckBox2.Checked = true;
                CheckBox3.Checked = true;
                CheckBox4.Checked = true;
                CheckBox5.Checked = true;
                CheckBox6.Checked = true;
                CheckBox7.Checked = true;
                CheckBox8.Checked = true;
                CheckBox9.Checked = true;
                CheckBox10.Checked = true;
                CheckBox11.Checked = true;
                CheckBox12.Checked = true;
                CheckBox13.Checked = true;
                CheckBox14.Checked = true;

                TextBox4.Text = "1";
                TextBox5.Text = "365";

                TextBox12.Text = DateTime.Now.ToString("M/d/yyyy");
                TextBox13.Text = DateTime.Now.ToString("M/d/yyyy");

                CheckBox16.Visible = false;
                CheckBox17.Visible = false;
                CheckBox18.Visible = false;
                CheckBox19.Visible = false;
  //*****************************************************************************************
                DataRow dr = PW.dtReqRates.Rows[0];

                ////PW.location_id = Convert.ToInt32(dr[1].ToString());
                ////PW.Reports = "1";
                //////DropDownList1.Items.FindByValue("Toronto Valet").Selected = true;
                ////daRR.Fill(dtRR, TextBox1.Text, PW.location_id);
                ////Label19.Text = dtRR.Rows[0].ItemArray[3].ToString();
                ////Label20.Text = dtRR.Rows[0].ItemArray[4].ToString();
                ////Label21.Text = dtRR.Rows[0].ItemArray[5].ToString();
                ////Label22.Text = dtRR.Rows[0].ItemArray[6].ToString();
                ////TextBox6.Text = dtRR.Rows[0].ItemArray[3].ToString();
                ////TextBox7.Text = dtRR.Rows[0].ItemArray[4].ToString();
                ////TextBox8.Text = dtRR.Rows[0].ItemArray[5].ToString();
                ////TextBox9.Text = dtRR.Rows[0].ItemArray[6].ToString();

                //TextBox1.Text = dr[6].ToString();
                ////TextBox2.Text = "1234567";
                ////TextBox3.Text = "1234567";

                //#region CheckBoxes1
                //string strStay = dr[7].ToString();

                //CheckBox1.Checked = false;
                //CheckBox2.Checked = false;
                //CheckBox3.Checked = false;
                //CheckBox4.Checked = false;
                //CheckBox5.Checked = false;
                //CheckBox6.Checked = false;
                //CheckBox7.Checked = false;
                //if (strStay.Contains("Mo"))
                //{
                //    CheckBox1.Checked = true;
                //}
                //if (strStay.Contains("Tu"))
                //{
                //    CheckBox2.Checked = true;
                //}
                //if (strStay.Contains("We"))
                //{
                //    CheckBox3.Checked = true;
                //}
                //if (strStay.Contains("Th"))
                //{
                //    CheckBox4.Checked = true;
                //}
                //if (strStay.Contains("Fr"))
                //{
                //    CheckBox5.Checked = true;
                //}
                //if (strStay.Contains("Sa"))
                //{
                //    CheckBox6.Checked = true;
                //}
                //if (strStay.Contains("Su"))
                //{
                //    CheckBox7.Checked = true;
                //}

                //string strPark = dr[8].ToString();

                //CheckBox8.Checked = false;
                //CheckBox9.Checked = false;
                //CheckBox10.Checked = false;
                //CheckBox11.Checked = false;
                //CheckBox12.Checked = false;
                //CheckBox13.Checked = false;
                //CheckBox14.Checked = false;
                //if (strPark.Contains("Mo"))
                //{
                //    CheckBox8.Checked = true;
                //}
                //if (strPark.Contains("Tu"))
                //{
                //    CheckBox9.Checked = true;
                //}
                //if (strPark.Contains("We"))
                //{
                //    CheckBox10.Checked = true;
                //}
                //if (strPark.Contains("Th"))
                //{
                //    CheckBox11.Checked = true;
                //}
                //if (strPark.Contains("Fr"))
                //{
                //    CheckBox12.Checked = true;
                //}
                //if (strPark.Contains("Sa"))
                //{
                //    CheckBox13.Checked = true;
                //}
                //if (strPark.Contains("Su"))
                //{
                //    CheckBox14.Checked = true;
                //}

                //#endregion
                //TextBox4.Text = dr[9].ToString();
                //TextBox5.Text = dr[10].ToString();

                //TextBox12.Text = Convert.ToDateTime(dr[3]).ToString("M/d/yyyy");
                //TextBox13.Text = Convert.ToDateTime(dr[4]).ToString("M/d/yyyy");

                //DropDownList1.ClearSelection(); //making sure the previous selection has been cleared
                //DropDownList1.Items.FindByValue(dr[1].ToString()).Selected = true;

//*****************************************************************************************
                Panel1.Visible = true;
                Panel2.Visible = false;

                CheckBox16.Visible = false;
                CheckBox17.Visible = false;
                CheckBox18.Visible = false;
                CheckBox19.Visible = false;
                txtPerc.Visible = false;
                Button3.Visible = false;
                Label31.Visible = false;

            }

            //txtqut.Attributes.Add("onkeyup", "CalcSellPrice2(" + CurPr + ", '" + txtqut.ClientID + "','" + lblTotal.ClientID + "')");  
            //Label lblCurrentListPrice = (Label)e.Row.FindControl("lblCurrentListPrice");
            //Double CurPr = Convert.ToDouble(lblCurrentListPrice.Text);
            //TextBox txtqut = (TextBox)e.Row.FindControl("txtqut");
            //Label lblTotal = (Label)e.Row.FindControl("lblTotal");

            //Label lblCurrentListPrice = (Label)e.Row.FindControl("lblCurrentListPrice");
            //Double CurPr = Convert.ToDouble(lblCurrentListPrice.Text);
            //TextBox txtqut = (TextBox)e.Row.FindControl("txtqut");
            //Label Label27 = (Label)Label27;

            DiscountInfoCalculation();

            Decimal jRRate1 = Convert.ToDecimal(Label19.Text);
            //Decimal jRRate1 = decimal.Round(Convert.ToDecimal(Label19.Text), 2);
            Decimal jRRate2 = Convert.ToDecimal(Label20.Text);
            //Decimal jRRate2 = decimal.Round(Convert.ToDecimal(Label20.Text), 2);
            Decimal jRRate3 = Convert.ToDecimal(Label21.Text);
            //Decimal jRRate3 = decimal.Round(Convert.ToDecimal(Label21.Text), 2);
            Decimal jRRate4 = Convert.ToDecimal(Label22.Text);


            TextBox6.Attributes.Add("onkeyup", "CalcDiscount('" + Label27.ClientID + "','" + TextBox6.ClientID + "', " + jRRate1 + ", 'H'," + DropDownList3.Text + " )");
            TextBox7.Attributes.Add("onkeyup", "CalcDiscount('" + Label28.ClientID + "','" + TextBox7.ClientID + "', " + jRRate2 + ", 'D'," + DropDownList3.Text + " )");
            TextBox8.Attributes.Add("onkeyup", "CalcDiscount('" + Label29.ClientID + "','" + TextBox8.ClientID + "', " + jRRate3 + ", 'W'," + DropDownList3.Text + " )");
            TextBox9.Attributes.Add("onkeyup", "CalcDiscount('" + Label30.ClientID + "','" + TextBox9.ClientID + "', " + jRRate4 + ", 'M'," + DropDownList3.Text + " )");

            


            if (Session["indA"].ToString() != "1")
            {

                string close = @"<script type='text/javascript'>
                                window.returnValue = true; 
                                window.close();
                                </script>";
                base.Response.Write(close);

            }

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            sdate = Convert.ToDateTime(TextBox12.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
            sdate = sdate.AddSeconds(1);

            PW.location_id = Convert.ToInt32(DropDownList1.SelectedItem.Value);
            Label32.Text = DropDownList1.SelectedItem.Value;
            daRR.Fill(dtRR, TextBox1.Text, PW.location_id, sdate);
            Label19.Text = dtRR.Rows[0].ItemArray[3].ToString();
            Label20.Text = dtRR.Rows[0].ItemArray[4].ToString();
            Label21.Text = dtRR.Rows[0].ItemArray[5].ToString();
            Label22.Text = dtRR.Rows[0].ItemArray[6].ToString();
            if (DropDownList3.Text == "1")
            {
                TextBox6.Text = dtRR.Rows[0].ItemArray[3].ToString();
            }
            if (DropDownList3.Text == "2")
            {
                TextBox6.Text = "0.00";
            }
            if (DropDownList3.Text == "3")
            {
                TextBox6.Text = "0";
            }

            TextBox7.Text = dtRR.Rows[0].ItemArray[4].ToString();
            TextBox8.Text = dtRR.Rows[0].ItemArray[5].ToString();
            TextBox9.Text = dtRR.Rows[0].ItemArray[6].ToString();
            Label27.Text = "0 (0%)";
            Label28.Text = "0 (0%)";
            Label29.Text = "0 (0%)";
            Label30.Text = "0 (0%)";


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //dtReqRates.Columns.Add("RHRate", typeof(String));
            //dtReqRates.Columns.Add("RDRate", typeof(String));
            //dtReqRates.Columns.Add("RWRate", typeof(String));
            //dtReqRates.Columns.Add("RMRate", typeof(String));

            string strStay = "";
            string strPark = "";
            string strStayNum = "";
            string strParkNum = "";
            int iStay = 0;
            int iPark = 0;
            //DataRow dr = PW.dtReqRates.NewRow();
            DataRow dr = PW.dtReqRates.Rows[0];
            
            //dr[0] = 1;
            //dr[1] = PW.location_id;
            dr[1] = Convert.ToInt32(DropDownList1.SelectedItem.Value);
            dr[2] = DropDownList2.SelectedItem.Text;
            //dr[3] = DateTime.Parse(Request.Form[TextBox12.UniqueID]);
            //dr[4] = DateTime.Parse(Request.Form[TextBox13.UniqueID]);
            //dr[3] = DateTime.Parse(TextBox12.Text);
            //dr[4] = DateTime.Parse(TextBox13.Text);
            dr[3] = Convert.ToDateTime(TextBox12.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
            dr[4] = Convert.ToDateTime(TextBox13.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);

            sdate = Convert.ToDateTime(TextBox12.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);

            dr[5] = DropDownList3.SelectedItem.Text;
            dr[6] = TextBox1.Text;

            //dr[7] = TextBox2.Text;
           // dr[8] = TextBox3.Text;
            if (CheckBox7.Checked)
            {
                strStayNum = strStayNum + "1";
            }

            if (CheckBox1.Checked)
            {
                strStay = strStay + "Mo,";
                strStayNum = strStayNum + "2";
            }
            if (CheckBox2.Checked)
            {
                strStay = strStay + "Tu,";
                strStayNum = strStayNum + "3";
            }
            if (CheckBox3.Checked)
            {
                strStay = strStay + "We,";
                strStayNum = strStayNum + "4";

            }
            if (CheckBox4.Checked)
            {
                strStay = strStay + "Th,";
                strStayNum = strStayNum + "5";
            }
            if (CheckBox5.Checked)
            {
                strStay = strStay + "Fr,";
                strStayNum = strStayNum + "6";
            }
            if (CheckBox6.Checked)
            {
                strStay = strStay + "Sa,";
                strStayNum = strStayNum + "7";
            }
            if (CheckBox7.Checked)
            {
                strStay = strStay + "Su,";

            }

            dr[7] = strStay.Substring(0, strStay.Length - 1);
            iStay = Convert.ToInt32(strStayNum);
            PW.iStay = iStay;

            if (CheckBox14.Checked)
            {
                strParkNum = strParkNum + "1";
            }
            if (CheckBox8.Checked)
            {
                strPark = strPark + "Mo,";
                strParkNum = strParkNum + "2";
            }
            if (CheckBox9.Checked)
            {
                strPark = strPark + "Tu,";
                strParkNum = strParkNum + "3";
            }
            if (CheckBox10.Checked)
            {
                strPark = strPark + "We,";
                strParkNum = strParkNum + "4";
            }
            if (CheckBox11.Checked)
            {
                strPark = strPark + "Th,";
                strParkNum = strParkNum + "5";
            }
            if (CheckBox12.Checked)
            {
                strPark = strPark + "Fr,";
                strParkNum = strParkNum + "6";
            }
            if (CheckBox13.Checked)
            {
                strPark = strPark + "Sa,";
                strParkNum = strParkNum + "7";
            }
            if (CheckBox14.Checked)
            {
                strPark = strPark + "Su,";
            }

            dr[8] = strPark.Substring(0, strPark.Length - 1);
            iPark = Convert.ToInt32(strParkNum);
            PW.iPark = iPark;

            dr[9] = TextBox4.Text;
            dr[10] = TextBox5.Text;
            dr[11] = TextBox6.Text;
            dr[12] = TextBox7.Text;
            dr[13] = TextBox8.Text;
            dr[14] = TextBox9.Text;
            if (TextBox10.Text.Length > 0)
	        {
		        dr[15] = TextBox10.Text;
	        }            
            dr[16] = TextBox11.Text;
            dr[17] = Session["UserName"];
            dr[18] = DateTime.Now;

            dr["RHRate"] = Label19.Text;
            dr["RDRate"] = Label20.Text;
            dr["RWRate"] = Label21.Text;
            dr["RMRate"] = Label22.Text;
            dr["location_name"] = DropDownList1.SelectedItem.Text;

            try
            {
                SaveRequestData();
            }
            catch (Exception)
            {
                dr[0] = 0;
            }

           
//*************************BarCode**********************************************************
            //bar.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            //bar.IncludeLabel = true;
            //bar.RotateFlipType = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            string strReqId = dr[0].ToString();
            //int n0 = 10 - strReqId.Length;
            //string strBar = strReqId;
            //for (int i = 0; i < n0; i++)
            //{
            //    strBar = "0" + strBar;
            //}

            //b = bar.Encode(BarcodeLib.TYPE.CODE39Extended,strBar , 300, 60);
            ////dr[0].ToString()
            //dr["imgID"] = ConvertImageToByteArray(b, System.Drawing.Imaging.ImageFormat.Jpeg);
//**************************************************************************************************

            //**********************QR Barcode**********************************************************
            QrEncoder encoder = new QrEncoder(ErrorCorrectionLevel.M);
            QrCode qrCode;
            encoder.TryEncode(strReqId, out qrCode);

            WriteableBitmapRenderer wRenderer = new WriteableBitmapRenderer(
                new FixedModuleSize(2, QuietZoneModules.Two),
                Colors.Black, Colors.White);

            WriteableBitmap wBitmap = new WriteableBitmap(50, 50, 96, 96, PixelFormats.Gray8, null);
            wRenderer.Draw(wBitmap, qrCode.Matrix);

            MemoryStream ms = new MemoryStream();
            wRenderer.WriteToStream(qrCode.Matrix, ImageFormatEnum.JPEG, ms);

            //You can also use above wBitmap to encode to image file on your own. 
            MemoryStream wms = new MemoryStream();
            //PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
            //pngEncoder.Interlace = PngInterlaceOption.On;
            //pngEncoder.Frames.Add(BitmapFrame.Create(wBitmap));
            //pngEncoder.Save(wms);

            JpegBitmapEncoder jpgEncoder = new JpegBitmapEncoder();
            jpgEncoder.Frames.Add(BitmapFrame.Create(wBitmap));
            jpgEncoder.Save(wms);

            //pictureBox1.Image = Image.FromStream(wms, true);

            dr["imgID"] = ConvertImageToByteArray(System.Drawing.Image.FromStream(wms, true), System.Drawing.Imaging.ImageFormat.Jpeg);

            //******************************************************************************************


            //PW.dtReqRates.Rows.Add(dr);

            //Response.Redirect("NewRatesRequest.aspx");

            Panel1.Visible = false;
            Panel2.Visible = true;

            ReportViewer1.Visible = true;
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.Refresh();

            if (DropDownList3.Text == "1")
            {
                ReportViewer1.LocalReport.ReportPath = @"NewRatesRequest.rdlc";               
            }
            if (DropDownList3.Text == "2")
            {
                ReportViewer1.LocalReport.ReportPath = @"NewRatesRequestFlat.rdlc";               
            }
            if (DropDownList3.Text == "3")
            {
                ReportViewer1.LocalReport.ReportPath = @"NewRatesRequestDays.rdlc";               
            }


            //ReportViewer1.LocalReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\pnfw2\pnfw1\Report1.rdlc";


            Microsoft.Reporting.WebForms.ReportDataSource dsNewRRequest = new Microsoft.Reporting.WebForms.ReportDataSource();
            dsNewRRequest.Value = PW.dtReqRates;
            dsNewRRequest.Name = "dsNRRequest";
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(dsNewRRequest);
            ReportViewer1.LocalReport.Refresh();

            ReportViewer1.LocalReport.DisplayName = dr[30].ToString().Replace(" ","") + dr[6].ToString() + "_" + Convert.ToDateTime(dr[3]).ToString("MMddyy")+ "_" + Convert.ToDateTime(dr[4]).ToString("MMddyy");

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Label27.Text = HiddenField1.Value;
            Label28.Text = HiddenField2.Value;
            Label29.Text = HiddenField3.Value;
            Label30.Text = HiddenField4.Value;

            DiscountInfoCalculation();

        }

        protected void TextBox12_TextChanged(object sender, EventArgs e)
        {
            sdate = Convert.ToDateTime(TextBox12.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);

            PW.location_id = Convert.ToInt32(DropDownList1.SelectedItem.Value);
            daRR.Fill(dtRR, TextBox1.Text, PW.location_id, sdate);
            Label19.Text = dtRR.Rows[0].ItemArray[3].ToString();
            Label20.Text = dtRR.Rows[0].ItemArray[4].ToString();
            Label21.Text = dtRR.Rows[0].ItemArray[5].ToString();
            Label22.Text = dtRR.Rows[0].ItemArray[6].ToString();
            TextBox6.Text = dtRR.Rows[0].ItemArray[3].ToString();
            TextBox7.Text = dtRR.Rows[0].ItemArray[4].ToString();
            TextBox8.Text = dtRR.Rows[0].ItemArray[5].ToString();
            TextBox9.Text = dtRR.Rows[0].ItemArray[6].ToString();
            Label27.Text = "0 (0%)";
            Label28.Text = "0 (0%)";
            Label29.Text = "0 (0%)";
            Label30.Text = "0 (0%)";

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (CheckBox16.Checked == true)
            {
                TextBox6.Text = (Convert.ToDecimal(Label19.Text) - Math.Round((Convert.ToDecimal(Label19.Text) * Convert.ToDecimal(txtPerc.Text))/100, 2)).ToString();
            }
            if (CheckBox17.Checked == true)
            {
                TextBox7.Text = (Convert.ToDecimal(Label20.Text) - Math.Round((Convert.ToDecimal(Label20.Text) * Convert.ToDecimal(txtPerc.Text)) / 100, 2)).ToString();
            }
            if (CheckBox18.Checked == true)
            {
                TextBox8.Text = (Convert.ToDecimal(Label21.Text) - Math.Round((Convert.ToDecimal(Label21.Text) * Convert.ToDecimal(txtPerc.Text)) / 100, 2)).ToString();
            }
            if (CheckBox19.Checked == true)
            {
                TextBox9.Text = (Convert.ToDecimal(Label22.Text) - Math.Round((Convert.ToDecimal(Label22.Text) * Convert.ToDecimal(txtPerc.Text)) / 100, 2)).ToString();
            }

            DiscountInfoCalculation();

        }

        protected void CheckBox15_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox15.Checked == true)
            {
                CheckBox16.Visible = true;
                CheckBox17.Visible = true;
                CheckBox18.Visible = true;
                CheckBox19.Visible = true;
                CheckBox16.Checked = false;
                CheckBox17.Checked = true;
                CheckBox18.Checked = true;
                CheckBox19.Checked = true;
                txtPerc.Visible = true;
                Button3.Visible = true;
                Label31.Visible = true;
            }
            else
            {
                CheckBox16.Visible = false;
                CheckBox17.Visible = false;
                CheckBox18.Visible = false;
                CheckBox19.Visible = false;
                txtPerc.Visible = false;
                Button3.Visible = false;
                Label31.Visible = false;
            }
        }

        protected void DiscountInfoCalculation()
        {
            Decimal jRRate1 = Convert.ToDecimal(Label19.Text);
            //Decimal jRRate1 = decimal.Round(Convert.ToDecimal(Label19.Text), 2);
            Decimal jRRate2 = Convert.ToDecimal(Label20.Text);
            //Decimal jRRate2 = decimal.Round(Convert.ToDecimal(Label20.Text), 2);
            Decimal jRRate3 = Convert.ToDecimal(Label21.Text);
            //Decimal jRRate3 = decimal.Round(Convert.ToDecimal(Label21.Text), 2);
            Decimal jRRate4 = Convert.ToDecimal(Label22.Text);

            Decimal cRate1 = Convert.ToDecimal(TextBox6.Text);
            Decimal cRate2 = Convert.ToDecimal(TextBox7.Text);
            Decimal cRate3 = Convert.ToDecimal(TextBox8.Text);
            Decimal cRate4 = Convert.ToDecimal(TextBox9.Text);

            Decimal dRate1 = (jRRate1 - cRate1);
            Decimal dRate2 = (jRRate2 - cRate2);
            Decimal dRate3 = (jRRate3 - cRate3);
            Decimal dRate4 = (jRRate4 - cRate4);

            Decimal pRate1 = dRate1 * 100 / jRRate1;
            Decimal pRate2 = dRate2 * 100 / jRRate2;
            Decimal pRate3 = dRate3 * 100 / jRRate3;
            Decimal pRate4 = dRate4 * 100 / jRRate4;

            Label27.Text = Math.Round(dRate1, 2).ToString("G29") + " (" + Math.Round(pRate1, 2).ToString("G29") + "%)";
            Label28.Text = Math.Round(dRate2, 2).ToString("G29") + " (" + Math.Round(pRate2, 2).ToString("G29") + "%)";
            Label29.Text = Math.Round(dRate3, 2).ToString("G29") + " (" + Math.Round(pRate3, 2).ToString("G29") + "%)";
            Label30.Text = Math.Round(dRate4, 2).ToString("G29") + " (" + Math.Round(pRate4, 2).ToString("G29") + "%)"; 

        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList3.Text == "1")
            {
                Label13.Text = "HOUR";
                Label14.Visible = true;
                Label15.Visible = true;
                Label16.Visible = true;
                Label18.Visible = true;
                Label19.Visible = true;
                Label20.Visible = true;
                Label21.Visible = true;
                Label22.Visible = true;
                Label24.Text = "RATE";
                Label26.Visible = true;
                Label27.Visible = true;
                Label27.Text = "0 (0%)";
                Label28.Visible = true;
                Label28.Text = "0 (0%)";
                Label29.Visible = true;
                Label29.Text = "0 (0%)";
                Label30.Visible = true;
                Label30.Text = "0 (0%)";
                TextBox6.Text = Label19.Text;
                TextBox7.Visible = true;
                TextBox7.Text = Label20.Text;
                TextBox8.Visible = true;
                TextBox8.Text = Label21.Text;
                TextBox9.Visible = true;
                TextBox9.Text = Label22.Text;
                CheckBox15.Visible = true;
                CheckBox15.Checked = false;
            }
            if (DropDownList3.Text == "2")
            {
                Label13.Text = "FLAT RATE";
                Label14.Visible = false;
                Label15.Visible = false;
                Label16.Visible = false;
                Label18.Visible = false;
                Label19.Visible = false;
                Label20.Visible = false;
                Label21.Visible = false;
                Label22.Visible = false;
                Label24.Text = "RATE";
                Label26.Visible = false;
                Label27.Visible = false;
                Label28.Visible = false;
                Label29.Visible = false;
                Label30.Visible = false;
                TextBox6.Text = "0.00";
                TextBox7.Visible = false;
                TextBox8.Visible = false;
                TextBox9.Visible = false;
                CheckBox15.Visible = false;
                CheckBox16.Visible = false;
                CheckBox17.Visible = false;
                CheckBox18.Visible = false;
                CheckBox19.Visible = false;
                txtPerc.Visible = false;
                Label31.Visible = false;
                Button3.Visible = false;
            }
            if (DropDownList3.Text == "3")
            {
                Label13.Text = "FREE DAYS";
                Label14.Visible = false;
                Label15.Visible = false;
                Label16.Visible = false;
                Label18.Visible = false;
                Label19.Visible = false;
                Label20.Visible = false;
                Label21.Visible = false;
                Label22.Visible = false;
                Label24.Text = "QUANTITY";
                Label26.Visible = false;
                Label27.Visible = false;
                Label28.Visible = false;
                Label29.Visible = false;
                Label30.Visible = false;
                TextBox6.Text = "0";
                TextBox7.Visible = false;
                TextBox8.Visible = false;
                TextBox9.Visible = false;
                CheckBox15.Visible = false;
                CheckBox16.Visible = false;
                CheckBox17.Visible = false;
                CheckBox18.Visible = false;
                CheckBox19.Visible = false;
                txtPerc.Visible = false;
                Label31.Visible = false;
                Button3.Visible = false;
            }
        }

        protected void Button6_Click(object sender, EventArgs e)
        {

        }

        protected void SaveRequestData()
        {

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["PARKER_OCCURRENCESConnectionString"].ConnectionString)) 
            {
                DataRow dr = PW.dtReqRates.Rows[0];

                using (SqlCommand cmd = new SqlCommand("p_Save_rates_request_ATwr", conn)) 
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Author", dr[17]);
                    cmd.Parameters.AddWithValue("@DateOfRequest", dr[18]);
                    cmd.Parameters.AddWithValue("@LocationId", dr[1]);
                    cmd.Parameters.AddWithValue("@WebPaper", dr[2]);
                    cmd.Parameters.AddWithValue("@OpenDate", dr[3]);
                    cmd.Parameters.AddWithValue("@CloseDate", dr[4]);
                    cmd.Parameters.AddWithValue("@DiscountType", dr[5]);
                    cmd.Parameters.AddWithValue("@ReferralId", dr[6]);
                    cmd.Parameters.AddWithValue("@MinDays", dr[9]);
                    cmd.Parameters.AddWithValue("@MaxDays", dr[10]);
                    cmd.Parameters.AddWithValue("@DaysOfWeekParked", PW.iPark);
                    cmd.Parameters.AddWithValue("@DaysOfWeekStayed", PW.iStay);
                    cmd.Parameters.AddWithValue("@HourlyRate", dr[11]);
                    cmd.Parameters.AddWithValue("@DailyRate", dr[12]);
                    cmd.Parameters.AddWithValue("@WeeklyRate", dr[13]);
                    cmd.Parameters.AddWithValue("@MonthlyRate", dr[14]);
                    cmd.Parameters.AddWithValue("@Upgrade", dr[15]);
                    cmd.Parameters.AddWithValue("@SpecialNotes", dr[16]);
                    cmd.Parameters.Add("@NewId", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                    conn.Open();

                    cmd.ExecuteNonQuery();

                    dr[0] = Convert.ToInt32(cmd.Parameters["@NewId"].Value);
                }

            }
        }
    }
}