﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.Services;

namespace pnfw1
{

    public class Address
    {
        public string Address1 { get; set; }
        public string Apt { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string LocationId { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Site { get; set; }

    }

    public partial class Referrals : System.Web.UI.Page
    {
        dsReferrals.dtReferralsDataTable dtRef = new dsReferrals.dtReferralsDataTable();
        dsReferralsTableAdapters.dtReferralsTableAdapter daRef = new dsReferralsTableAdapters.dtReferralsTableAdapter();
        dsSegment.dtReferralSegmentsDataTable dtRefSegm = new dsSegment.dtReferralSegmentsDataTable();
        dsSegmentTableAdapters.dtReferralSegmentsTableAdapter daRefSegm = new dsSegmentTableAdapters.dtReferralSegmentsTableAdapter();
        dsMasters.dtMastersDataTable dtMastrs = new dsMasters.dtMastersDataTable();
        dsMastersTableAdapters.dtMastersTableAdapter daMastrs = new dsMastersTableAdapters.dtMastersTableAdapter();
        dsComPrograms.dtComProgramsDataTable dtCProgs = new dsComPrograms.dtComProgramsDataTable();
        dsComProgramsTableAdapters.dtComProgramsTableAdapter daCProgs = new dsComProgramsTableAdapters.dtComProgramsTableAdapter();
        dsSalesPersons.dtSalesEmployeesDataTable dtSalesPersons = new dsSalesPersons.dtSalesEmployeesDataTable();
        dsSalesPersonsTableAdapters.dtSalesEmployeesTableAdapter daSalesPersons = new dsSalesPersonsTableAdapters.dtSalesEmployeesTableAdapter();
        dsSelectRefAddress.dtRefAddressDataTable dtRefAddr = new dsSelectRefAddress.dtRefAddressDataTable();
        dsSelectRefAddressTableAdapters.dtRefAddressTableAdapter daRefAddr = new dsSelectRefAddressTableAdapters.dtRefAddressTableAdapter();
        dsCity.dtCitiesDataTable dtCitiesI = new dsCity.dtCitiesDataTable();
        dsCityTableAdapters.dtCitiesTableAdapter daCitiesI = new dsCityTableAdapters.dtCitiesTableAdapter();
        dsLocations.locationsDataTable dtLocs = new dsLocations.locationsDataTable();
        dsLocationsTableAdapters.locationsTableAdapter daLocs = new dsLocationsTableAdapters.locationsTableAdapter();
        dsUserApp.dtUserAppAllowDataTable dtUAA = new dsUserApp.dtUserAppAllowDataTable();
        dsUserAppTableAdapters.dtUserAppAllowTableAdapter daUAA = new dsUserAppTableAdapters.dtUserAppAllowTableAdapter();


        //int indicatorRef=2;

        int iUserAppR = 0;
        int iUserAppC = 0;

        //private const string ASCENDING = " ASC";
        //private const string DESCENDING = " DESC";

        //string strExpression = "referral_id";
        //string strDirection = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            DataTable dummy = new DataTable();
            dummy.Columns.Add("referral_id");
            dummy.Columns.Add("master_id");
            dummy.Columns.Add("segment_code");
            dummy.Columns.Add("referral_code");
            dummy.Columns.Add("iata");
            dummy.Columns.Add("name");
            dummy.Columns.Add("commission_program_id");
            dummy.Columns.Add("sales_person");
            dummy.Columns.Add("open_date");
            dummy.Columns.Add("close_date");
            dummy.Columns.Add("address_id");
            dummy.Columns.Add("market_code");
            dummy.Columns.Add("commission_split_perc");
            dummy.Columns.Add("contact");
            dummy.Columns.Add("allow_best_rate");
            dummy.Rows.Add();
            GridView1.DataSource = dummy;
            GridView1.DataBind();

            //Required for jQuery DataTables to work.
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "Hide1();", true);

            string strDirection = Session["Direction"].ToString();
            string strExpression = Session["Expression"].ToString();


            lblError.Visible = false;

            daUAA.Fill(dtUAA, Convert.ToInt32(Session["UserID"]), "WEB_REFERRALS_UPDATE");
            if (dtUAA.Rows.Count > 0)
            {
                iUserAppR = 1;
            }
            else
            {
                iUserAppR = 0;
            }

            string x = txtReferralId.Text;

            if (!IsPostBack)
            {
                try
                {
                    txtReferralId.Text = "0";

                    daRefSegm.Fill(dtRefSegm);
                    ddlSegmCode.DataSource = dtRefSegm;
                    ddlSegmCode.DataTextField = "description";
                    ddlSegmCode.DataValueField = "segment_code";
                    ddlSegmCode.DataBind();

                    daMastrs.Fill(dtMastrs);
                    ddlMaster.DataSource = dtMastrs;
                    ddlMaster.DataTextField = "name";
                    ddlMaster.DataValueField = "master_id";
                    ddlMaster.DataBind();

                    daCProgs.Fill(dtCProgs);
                    ddlProgram.DataSource = dtCProgs;
                    ddlProgram.DataTextField = "commission_rule";
                    ddlProgram.DataValueField = "commission_program_id";
                    ddlProgram.DataBind();

                    daSalesPersons.Fill(dtSalesPersons);
                    ddlSalesPerson.DataSource = dtSalesPersons;
                    ddlSalesPerson.DataTextField = "last_name";
                    ddlSalesPerson.DataValueField = "employee_id";
                    ddlSalesPerson.DataBind();

                    daCitiesI.Fill(dtCitiesI);
                    ddlCityI.DataSource = dtCitiesI;
                    ddlCityI.DataTextField = "city_name";
                    ddlCityI.DataValueField = "city_id";
                    ddlCityI.DataBind();
                    ddlCityI.SelectedValue = "2";

                    daLocs.Fill(dtLocs, (string)Session["Locations"]);
                    ddlLocation.DataSource = dtLocs;
                    ddlLocation.DataTextField = "location";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();

                    if (iUserAppR==1)
                    {
                        btnNewRef.Visible = true;
                    }
                    else
                    {
                        btnNewRef.Visible = false;
                    }

                    lblLocation.Visible = false;
                    ddlLocation.Visible = false; ;


                    GData.LogMessage(string.Format("Referrals.Page_Load. Success. User: {0}", Session["UserName"]));

                }
                catch (Exception objException)
                {
                    lblError.Visible = true;
                    Panel1.Visible = false;

                    // display it
                    GData.LogMessage(string.Format("Referrals.Page_Load. The error message is {0}. User: {1}", objException.Message, Session["UserName"]));

                }
            }

            if (iUserAppR == 0)
            {
                btnNewRef.Visible = false;
            }
            else
            {
                btnNewRef.Visible = true;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GData.LogMessage(string.Format("Referrals.ButtonLoad Clicked"));
        }

        protected void btnNewRef_Click(object sender, EventArgs e)
        {
            try
            {
                //int x = 1;
                //int y = 0;
                //int z = x / y;
                Panel2.Visible = true;
                Session["RefSC"] = "C";
                lblTitle1.Text = "New Referral Details";
                lblTitle2.Text = "New Address";

                lblLocation.Visible = true;
                ddlLocation.Visible = true;

                txtRefCode.Text = "";
                txtIata.Text = ""; ;
                txtMarkCode.Text = "";
                txtName.Text = "Name ? ";
                txtSplitPerc.Text = "";
                ddlMaster.SelectedValue = "";
                ddlProgram.SelectedValue = "";
                txtOpenDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtCloseDate.Text = "";
                ddlSalesPerson.SelectedValue = "";
                txtContact.Text = "";

                txtStreet.Text = "";
                txtNumber.Text = "";
                txtApt.Text = "";
                txtCity.Text = "";
                txtProv.Text = "";
                txtCountry.Text = "";
                txtPCode.Text = "";
                txtPhone.Text = "";
                txtFax.Text = "";
                txtSite.Text = "";

                GData.LogMessage(string.Format("Referrals.ButtonNewReferral Clicked"));

            }
            catch (Exception objException)
            {
                lblError.Visible = true;
                Panel1.Visible = false;

                // display it
                GData.LogMessage(string.Format("Referrals.ButtonNewReferral. The error message is {0}. User: {1}", objException.Message, Session["UserName"]));
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            GData.LogMessage(string.Format("Referrals.ButtonSaveCreate Clicked"));
        }

        protected void btnCreateCoupon_Click(object sender, EventArgs e)
        {
            GData.LogMessage(string.Format("Referrals.ButtonCreateCoupon Clicked"));
        }   


        [WebMethod]
        public static List<Referral> GetReferrals(string rID)
        {
            List<Referral> referrals = new List<Referral>();

            if (rID == "0")
            {
                rID = "";
            }

            string constr = ConfigurationManager.ConnectionStrings["PARKER_ALLConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                //using (SqlCommand cmd = new SqlCommand("SELECT [referral_id],[master_id],[segment_code] ,[referral_code],[iata] ,[name],[commission_program_id]," +
                //    "[sales_person] ,convert(varchar, [open_date], 101) AS 'open_date', convert(varchar, [close_date], 101) AS 'close_date',[address_id], " +
                //    "[market_code],[commission_split_perc],[contact],[allow_best_rate] FROM[dbo].[referrals]", con))
                using (SqlCommand cmd = new SqlCommand("p_Select_referrals_ATw '" + rID + "' ,1", con))
                {
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            referrals.Add(new Referral
                            {
                                referral_id = sdr["referral_id"].ToString(),
                                master_id = sdr["master_id"].ToString(),
                                segment_code = sdr["segment_code"].ToString(),
                                referral_code = sdr["referral_code"].ToString(),
                                iata = sdr["iata"].ToString(),
                                name = sdr["name"].ToString(),
                                commission_program_id = sdr["commission_program_id"].ToString(),
                                sales_person = sdr["sales_person"].ToString(),
                                open_date = sdr["open_date"].ToString(),
                                close_date = sdr["close_date"].ToString(),
                                address_id = sdr["address_id"].ToString(),
                                market_code = sdr["market_code"].ToString(),
                                commission_split_perc = sdr["commission_split_perc"].ToString(),
                                contact = sdr["contact"].ToString(),
                                allow_best_rate = sdr["allow_best_rate"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
            }

            return referrals;
        }

        public class Referral
        {
            public string referral_id { get; set; }
            public string master_id { get; set; }
            public string segment_code { get; set; }
            public string referral_code { get; set; }
            public string iata { get; set; }
            public string name { get; set; }
            public string commission_program_id { get; set; }
            public string sales_person { get; set; }
            public string open_date { get; set; }
            public string close_date { get; set; }
            public string address_id { get; set; }
            public string market_code { get; set; }
            public string commission_split_perc { get; set; }
            public string contact { get; set; }
            public string allow_best_rate { get; set; }
        }

        [System.Web.Services.WebMethod]
        public static Address GetData(string ID)
        {
            Address address = new Address();
            string conStrA = ConfigurationManager.ConnectionStrings["PARKER_ALLConnectionString"].ToString();
            //using (SqlConnection oConn = new SqlConnection("Data Source=PNFSQLCLUSTER;Initial Catalog=PARKER_REPORTS;Integrated Security=Yes"))
            using (SqlConnection oConn = new SqlConnection(conStrA))
            {
                oConn.Open();
                SqlCommand oCmd = new SqlCommand("SELECT address_1, street_number, address_2, city, province, country, postal_code, location_id, " +
                "business_phone_number, fax, website  FROM addresses WHERE address_id = " + ID, oConn);
                SqlDataReader oReader = oCmd.ExecuteReader();
                while (oReader.Read() == true)
                {
                    //clsRow oRow = new clsRow();
                    address.Address1 = oReader["address_1"].ToString();
                    address.Address2 = oReader["street_number"].ToString();
                    address.Apt = oReader["address_2"].ToString();
                    address.City = oReader["city"].ToString();
                    address.Province = oReader["Province"].ToString();
                    address.Country = oReader["Country"].ToString();
                    address.PostalCode = oReader["postal_code"].ToString();
                    address.LocationId = oReader["location_id"].ToString();
                    address.Phone = oReader["business_phone_number"].ToString();
                    address.Fax = oReader["fax"].ToString();
                    address.Site = oReader["website"].ToString();
                }
                oReader.Close();
            }
            return address;
        }

        [WebMethod]
        public static string SaveCreateRefAdr(string RI, string MR, string SG, string RC, string IA, string MK, string NM, string PR, string SR, string SP, string OD, 
            string CD, string CN, string AD, string ST, string NU, string AP, string CI, string PV, string CO, string PC, string PH, string FA, string SI, string LO, string SC)
        {
            
            string strRefId = "Error";

            //DataTable dt = new DataTable();
            DateTime OpenDate;
            DateTime CloseDate;

            if (OD=="ZERO")
            {
                OD = "";
            }

            if (CD == "ZERO")
            {
                CD = "";
            }

            if (AD == "ZERO")
            {
                AD = "";
            }

           if (MK == "ZERO")
            {
                MK = "";
            }


            CultureInfo enUS = new CultureInfo("en-US");

            if (DateTime.TryParseExact(OD, "MM/dd/yyyy", enUS, DateTimeStyles.None, out OpenDate))
            {

                if ((DateTime.TryParseExact(CD, "MM/dd/yyyy", enUS, DateTimeStyles.None, out CloseDate)) || CD.Length == 0)
                {

                    int result = 9;
                    Int64 iAddressID;
                    int iReferralID;
                    string resultC2 = "";
                    string resultC1 = "";

                    try
                    {
                        //if ((string)Session["RefSC"] == "S")
                        if(SC=="S")
                        {
                            //                //txtRfrrlId.Text = txtReferralId.Text;

                            string conStr = ConfigurationManager.ConnectionStrings["PARKER_ALLConnectionString"].ToString();
                            using (SqlConnection conn = new SqlConnection(conStr))
                            {
                                using (SqlCommand cmd = conn.CreateCommand())
                                {
                                    cmd.CommandText = "p_Update_referrals_and_address_ATw";
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    //   cmd.Parameters.Add(new SqlParameter("@referral_id", Convert.ToInt32(GridView1.SelectedRow.Cells[0].Text.Replace("&nbsp;", ""))));

                                    cmd.Parameters.Add(new SqlParameter("@referral_id", Convert.ToInt32(RI)));


                                    if (MR !="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@master_id", Convert.ToInt32(MR)));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@master_id", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@segment_code", Convert.ToInt32(SG)));

                                    if (RC!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@referral_code", RC.TrimStart()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@referral_code", DBNull.Value));
                                    }

                                    if (IA!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@iata", IA.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@iata", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@name", NM));

                                    if (PR!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@commission_program_id", Convert.ToInt32(PR)));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@commission_program_id", DBNull.Value));
                                    }

                                    if (SR!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@sales_person", Convert.ToInt32(SR)));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@sales_person", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@open_date", Convert.ToDateTime(OD, enUS)));

                                    if (CD.Length != 0)
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@close_date", Convert.ToDateTime(CD, enUS)));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@close_date", DBNull.Value));
                                    }

                                    if (AD.Replace("&nbsp;", "").Length > 0)
                                    {
                                        // cmd.Parameters.Add(new SqlParameter("@address_id", Convert.ToInt64(GridView1.SelectedRow.Cells[10].Text)));
                                        cmd.Parameters.Add(new SqlParameter("@address_id", Convert.ToInt64(AD.Replace("&nbsp;", ""))));

                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_id", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@market_code", MK));

                                    if (SP!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@commission_split_perc", Convert.ToInt32(SP)));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@commission_split_perc", DBNull.Value));
                                    }

                                    if (CN!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@contact", CN));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@contact", DBNull.Value));
                                    }

                                    if (NU!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@street_number", NU.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@street_number", DBNull.Value));
                                    }

                                    if (ST!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_1", ST.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_1", DBNull.Value));
                                    }

                                    if (AP!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_2", AP.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_2", DBNull.Value));
                                    }

                                    if (CI!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@city", CI.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@city", DBNull.Value));
                                    }

                                    if (PV!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@province", PV.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@province", DBNull.Value));
                                    }

                                    if (CO!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@country", CO.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@country", DBNull.Value));
                                    }

                                    if (PC!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@postal_code", PC.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@postal_code", DBNull.Value));
                                    }

                                    if (PH!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@business_phone_number", PH.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@business_phone_number", DBNull.Value));
                                    }

                                    if (FA!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@fax", FA.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@fax", DBNull.Value));
                                    }

                                    if (SI!="ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@website", SI.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@website", DBNull.Value));
                                    }
                                    cmd.Parameters.Add("@Result", SqlDbType.Int);
                                    cmd.Parameters["@Result"].Direction = ParameterDirection.Output;

                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();

                                    result = Convert.ToInt32(cmd.Parameters["@Result"].Value.ToString());

                                    if (result == 0)
                                    {
                                        strRefId = RI;
                                    }
                                    else
                                    {
                                        strRefId = "10000000";
                                    }
                                }
                            }

                        }
                        else
                        {

                            string conStr = ConfigurationManager.ConnectionStrings["PARKER_ALLConnectionString"].ToString();
                            using (SqlConnection conn = new SqlConnection(conStr))
                            {
                                using (SqlCommand cmd = conn.CreateCommand())
                                using (SqlCommand cmd1 = conn.CreateCommand())
                                {
                                    cmd.CommandText = "p_Create_addresses";
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.Parameters.Add(new SqlParameter("@location_id", Convert.ToInt32(LO)));

                                    if (NU != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@street_number", NU.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@street_number", DBNull.Value));
                                    }

                                    if (ST != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_1", ST.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_1", DBNull.Value));
                                    }

                                    if (AP != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_2", AP.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@address_2", DBNull.Value));
                                    }

                                    if (CI != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@city", CI.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@city", DBNull.Value));
                                    }

                                    if (PV != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@province", PV.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@province", DBNull.Value));
                                    }

                                    if (CO != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@country", CO.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@country", DBNull.Value));
                                    }

                                    if (PC != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@postal_code", PC.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@postal_code", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@home_phone_number", DBNull.Value));
                                    cmd.Parameters.Add(new SqlParameter("@cell_phone_number", DBNull.Value));

                                    if (PH != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@business_phone_number", PH.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@business_phone_number", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@business_ext", DBNull.Value));

                                    if (FA != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@fax", FA.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@fax", DBNull.Value));
                                    }

                                    cmd.Parameters.Add(new SqlParameter("@email", DBNull.Value));

                                    if (SI != "ZERO")
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@website", SI.Trim()));
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add(new SqlParameter("@website", DBNull.Value));
                                    }

                                    cmd.Parameters.Add("@address_id", SqlDbType.BigInt);
                                    cmd.Parameters["@address_id"].Direction = ParameterDirection.Output;

                                    var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);
                                    returnParameter.Direction = ParameterDirection.ReturnValue;

                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    var result1 = returnParameter.Value;
                                    resultC1 = result1.ToString();

                                    iAddressID = Convert.ToInt64(cmd.Parameters["@address_id"].Value.ToString());


                                    cmd1.CommandText = "p_Create_referrals";
                                    cmd1.CommandType = CommandType.StoredProcedure;

                                    cmd1.Parameters.Add("@referral_id", SqlDbType.Int);
                                    cmd1.Parameters["@referral_id"].Direction = ParameterDirection.Output;

                                    if(MR != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@master_id", Convert.ToInt32(MR)));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@master_id", DBNull.Value));
                                    }

                                    cmd1.Parameters.Add(new SqlParameter("@segment_code", Convert.ToInt32(SG)));

                                    if (RC != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@referral_code", RC.TrimStart()));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@referral_code", DBNull.Value));
                                    }

                                    if(IA != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@iata", IA.Trim()));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@iata", DBNull.Value));
                                    }

                                    cmd1.Parameters.Add(new SqlParameter("@name", NM));

                                    if (PR != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@commission_program_id", Convert.ToInt32(PR)));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@commission_program_id", DBNull.Value));
                                    }

                                    if (SR != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@sales_person", Convert.ToInt32(SR)));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@sales_person", DBNull.Value));
                                    }

                                    cmd1.Parameters.Add(new SqlParameter("@open_date", Convert.ToDateTime(OD, enUS)));

                                    if (CD.Length != 0)
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@close_date", Convert.ToDateTime(CD, enUS)));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@close_date", DBNull.Value));
                                    }

                                    cmd1.Parameters.Add(new SqlParameter("@address_id", iAddressID));

                                    //cmd1.Parameters.Add(new SqlParameter("@referral_image", DBNull.Value));
                                    SqlParameter imageParameter = new SqlParameter("@referral_image", SqlDbType.Image);
                                    imageParameter.Value = DBNull.Value;
                                    cmd1.Parameters.Add(imageParameter);

                                    cmd1.Parameters.Add(new SqlParameter("@market_code", MK));


                                    if (SP != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@commission_split_perc", Convert.ToInt32(SP)));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@commission_split_perc", DBNull.Value));
                                    }

                                    if (CN != "ZERO")
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@contact", CN));
                                    }
                                    else
                                    {
                                        cmd1.Parameters.Add(new SqlParameter("@contact", DBNull.Value));
                                    }

                                    var returnParameter1 = cmd1.Parameters.Add("@ReturnVal", SqlDbType.Int);
                                    returnParameter1.Direction = ParameterDirection.ReturnValue;

                                    cmd1.ExecuteNonQuery();
                                    var result0 = returnParameter1.Value;
                                    resultC2 = result0.ToString();

                                    iReferralID = Convert.ToInt32(cmd1.Parameters["@referral_id"].Value.ToString());

                                    conn.Close();
                                }

                                strRefId = iReferralID.ToString();
                            }

                        }

                        GData.LogMessage(string.Format("Referrals.ButtonSaveCreate. Success. User: {0}, Parameters: ReferralID:{1}/ MasterID:{2}/ SegmentCode:{3}/ ReferralCode:{4}/ IATA:{5}/ " +
                           "Name:{6}/ CommissionProgramID:{7}/ SalesPerson:{8}/ OpenDate:{9}/ CloseDate:{10}/ AddressID:{11}/ MarketCode:{12}/ CommissionSplitPercent:{13}/ Contact:{14}/ " +
                            "LocationID:{15}/ StreetNumber:{16}/ Address1:{17}/ Address2:{18}/ City:{19}/ Province:{20}/ Country:{21}/ PostalCode:{22}/ BusinessPhoneNumber:{23}/ Fax:{24}/ Website:{25}/ Result:{26}/ " +
                            "Result1:{27}/ Result2:{28}/ SaveCreate: {29}",
                            GData.strUserName, RI, MR, SG, RC, IA, NM, PR, SR, OD, CD, AD, MK, SP, CN, LO, NU, ST, AP, CI, PV, CO, PC, PH, FA, SI, result, resultC1, resultC2, SC));

                    }
                    catch (Exception objException)
                    {
                        GData.LogMessage(string.Format("Referrals.ButtonSaveCreate. ERROR. User: {0}, Parameters: ReferralID:{1}/ MasterID:{2}/ SegmentCode:{3}/ ReferralCode:{4}/ IATA:{5}/ " +
                           "Name:{6}/ CommissionProgramID:{7}/ SalesPerson:{8}/ OpenDate:{9}/ CloseDate:{10}/ AddressID:{11}/ MarketCode:{12}/ CommissionSplitPercent:{13}/ Contact:{14}/ " +
                            "LocationID:{15}/ StreetNumber:{16}/ Address1:{17}/ Address2:{18}/ City:{19}/ Province:{20}/ Country:{21}/ PostalCode:{22}/ BusinessPhoneNumber:{23}/ Fax:{24}/ Website:{25}/ Result:{26}/ " +
                            "Result1:{27}/ Result2:{28}/ SaveCreate: {29}/ The error message: {30}",
                            GData.strUserName, RI, MR, SG, RC, IA, NM, PR, SR, OD, CD, AD, MK, SP, CN, LO, NU, ST, AP, CI, PV, CO, PC, PH, FA, SI, result, resultC1, resultC2, SC, objException.Message));
                    }
                }
                else
                {
                    //lblWarningDates.Text = "CLOSE DATE IS NOT IN CORRECT FORMAT";
                    //lblWarningDates.Visible = true;
                }
            }
            else
            {
                //lblWarningDates.Text = "OPEN DATE IS NOT IN CORRECT FORMAT";
                //lblWarningDates.Visible = true;
            }

            return strRefId;
        }

        [WebMethod]
        public static string CreateCpn(string RI, string DI, string TR, string NL, string CI) 
        {

            string resultV = "";
            try
            {
                string conStr = ConfigurationManager.ConnectionStrings["PARKER_ALLConnectionString"].ToString();
                using (SqlConnection conn = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = conn.CreateCommand())
                    {

                        cmd.CommandText = "p_Create_Coupons_from_coupon_program_parameters_ATw";
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (RI != "ZERO")
                        {
                            cmd.Parameters.Add(new SqlParameter("@referral_id", Convert.ToInt32(RI)));
                        }

                        cmd.Parameters.Add(new SqlParameter("@ta_rates", TR));
                        cmd.Parameters.Add(new SqlParameter("@national_local", NL));
                        cmd.Parameters.Add(new SqlParameter("@city_id", CI));

                        if (DI != "ZERO")
                        {
                            cmd.Parameters.Add(new SqlParameter("@discount_id", Convert.ToInt32(DI)));
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@discount_id", DBNull.Value));
                        }

                        var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        conn.Open();
                        cmd.ExecuteNonQuery();
                        var result = returnParameter.Value;
                        resultV = result.ToString();

                        conn.Close();
                    }
                }

                GData.LogMessage(string.Format("Referrals.ButtonCreateCoupon. Success. User: {0}. Parameters: ReferralID:{1}/ TARates:{2}/ NatinalLocal:{3}/ CityID:{4}/ DiscountID:{5}/ " +
                    "ReturnVal:{6}", GData.strUserName, RI, TR, NL, CI, DI, resultV));

            }
            catch (Exception objException)
            {

                GData.LogMessage(string.Format("Referrals.ButtonCreateCoupon. ERROR. User: {0}. Parameters: ReferralID:{1}/ TARates:{2}/ NatinalLocal:{3}/ CityID:{4}/ DiscountID:{5}/ " +
                    "ReturnVal:{6}/ The error message: {7}", GData.strUserName, RI, TR, NL, CI, DI, resultV, objException.Message));

                //lblError.Visible = true;
                //Panel1.Visible = false;
                //Panel2.Visible = false;

            }

            return resultV;
        }


    }
}