﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace pnfw1
{
    public class PW
    {
        private static string _UserID = "";
        private static string _UserName = "";
        private static string _UserLogin = "";
        private static string _Locations = "";
        private static string _Reports = "";


        public static string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        public static string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        public static string UserLogin
        {
            get { return _UserLogin; }
            set { _UserLogin = value; }
        }

        public static string Locations
        {
            get { return _Locations; }
            set { _Locations = value; }
        }

        public static string Reports
        {
            get { return _Reports; }
            set { _Reports = value; }
        }

        public static int indB { set; get; }
        public static int indA { set; get; }

        public static int location_id { set; get; }
        public static string location_general { set; get; }

        public static dsRateRequests.dtRateRequestsDataTable dtReqRates = new dsRateRequests.dtRateRequestsDataTable();

        public static int iStay { set; get; }
        public static int iPark { set; get; }

    }
 
    public partial class _default : System.Web.UI.Page
    {

        dsWebCoupons.p_Reports_list_web_couponsDataTable dtWC = new dsWebCoupons.p_Reports_list_web_couponsDataTable();
        dsWebCouponsTableAdapters.p_Reports_list_web_couponsTableAdapter daWC = new dsWebCouponsTableAdapters.p_Reports_list_web_couponsTableAdapter();

        dsPaperCoupons.p_Reports_list_paper_coupons_date_ATwrDataTable dtPC = new dsPaperCoupons.p_Reports_list_paper_coupons_date_ATwrDataTable();
        dsPaperCouponsTableAdapters.p_Reports_list_paper_coupons_date_ATwrTableAdapter daPC = new dsPaperCouponsTableAdapters.p_Reports_list_paper_coupons_date_ATwrTableAdapter();

        dsCheckCoupon.p_Reports_check_paper_coupon_date_ATwrDataTable dtCC = new dsCheckCoupon.p_Reports_check_paper_coupon_date_ATwrDataTable();
        dsCheckCouponTableAdapters.p_Reports_check_paper_coupon_date_ATwrTableAdapter daCC = new dsCheckCouponTableAdapters.p_Reports_check_paper_coupon_date_ATwrTableAdapter();

        dsLocations.locationsDataTable dtLocations = new dsLocations.locationsDataTable();
        dsLocationsTableAdapters.locationsTableAdapter daLocations = new dsLocationsTableAdapters.locationsTableAdapter();

        dsRackRate.dtRackRateDataTable dtRR = new dsRackRate.dtRackRateDataTable();
        dsRackRateTableAdapters.dtRackRateTableAdapter daRR = new dsRackRateTableAdapters.dtRackRateTableAdapter();

        dsWebRatesGlance.dtWebRatesGlanceDataTable dtWRG = new dsWebRatesGlance.dtWebRatesGlanceDataTable();
        dsWebRatesGlanceTableAdapters.dtWebRatesGlanceTableAdapter daWRG = new dsWebRatesGlanceTableAdapters.dtWebRatesGlanceTableAdapter();

        dsTicketsSales.dtTicketsSalesDataTable dtTS = new dsTicketsSales.dtTicketsSalesDataTable();
        dsTicketsSalesTableAdapters.dtTicketsSalesTableAdapter daTS = new dsTicketsSalesTableAdapters.dtTicketsSalesTableAdapter();

        dsTicketsRevenue.dtTicketsRevenueDataTable dtTR = new dsTicketsRevenue.dtTicketsRevenueDataTable();
        dsTicketsRevenueTableAdapters.dtTicketsRevenueTableAdapter daTR = new dsTicketsRevenueTableAdapters.dtTicketsRevenueTableAdapter();

        dsExpiringWebDiscounts.dtExpiringWebDiscountsDataTable dtEWD = new dsExpiringWebDiscounts.dtExpiringWebDiscountsDataTable();
        dsExpiringWebDiscountsTableAdapters.dtExpiringWebDiscountsTableAdapter daEWD = new dsExpiringWebDiscountsTableAdapters.dtExpiringWebDiscountsTableAdapter();

        dsWEDCouponGroupsRevenue.dtWEDCouponGroupsRevenueDataTable dtWEDCGR = new dsWEDCouponGroupsRevenue.dtWEDCouponGroupsRevenueDataTable();
        dsWEDCouponGroupsRevenueTableAdapters.dtWEDCouponGroupsRevenueTableAdapter daWEDCGR = new dsWEDCouponGroupsRevenueTableAdapters.dtWEDCouponGroupsRevenueTableAdapter();

        dsWEDRates.dtWEDRatesDataTable dtWEDR = new dsWEDRates.dtWEDRatesDataTable();
        dsWEDRatesTableAdapters.dtWEDRatesTableAdapter daWEDR = new dsWEDRatesTableAdapters.dtWEDRatesTableAdapter();

        dsExpiringCouponsRevenue.dtExpiringCouponsRevenueDataTable dtECR = new dsExpiringCouponsRevenue.dtExpiringCouponsRevenueDataTable();
        dsExpiringCouponsRevenueTableAdapters.dtExpiringCouponsRevenueTableAdapter daECR = new dsExpiringCouponsRevenueTableAdapters.dtExpiringCouponsRevenueTableAdapter();

        dsOccurTickets.dtOccurToTicketsDataTable dtOT = new dsOccurTickets.dtOccurToTicketsDataTable();
        dsOccurTicketsTableAdapters.dtOccurToTicketsTableAdapter daOT = new dsOccurTicketsTableAdapters.dtOccurToTicketsTableAdapter();

        
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                PW.indB = 0;
          

                //ReportViewer1.Visible = false;

                daLocations.Fill(dtLocations, (string)Session["Locations"]);
                DropDownList2.DataSource = dtLocations;
                DropDownList2.DataTextField = "location";
                DropDownList2.DataValueField = "location_id";
                DropDownList2.DataBind();
                //ListItem l = new ListItem("Halifax", "113", true);
                //DropDownList2.Items.Add(l);

                ListItem i1 = new ListItem("ALL", "100,101,102,103,104,105,106,107,108,129,130", true);
                ListItem i2 = new ListItem("TORONTO", "100,101,102", true);
                ListItem i3 = new ListItem("Toronto Valet", "100", true);
                ListItem i4 = new ListItem("Toronto SelfPark", "101", true);
                ListItem i5 = new ListItem("Toronto Economy", "102", true);
                ListItem i6 = new ListItem("EDMONTON", "103", true);
                ListItem i7 = new ListItem("VANCOUVER", "104", true);
                ListItem i8 = new ListItem("MONTREAL", "105,106,107", true);
                ListItem i9 = new ListItem("Montreal Valet", "105", true);
                ListItem i10 = new ListItem("Montreal Express A", "106", true);
                ListItem i11 = new ListItem("Montreal Express B", "107", true);
                ListItem i12 = new ListItem("OTTAWA", "108", true);
                ListItem i13 = new ListItem("WINNIPEG", "129,130", true);
                ListItem i14 = new ListItem("Winnipeg Valet", "129", true);
                ListItem i15 = new ListItem("Winnipeg SelfPark", "130", true);

                DropDownList3.Items.Add(i1);
                DropDownList3.Items.Add(i2);
                DropDownList3.Items.Add(i3);
                DropDownList3.Items.Add(i4);
                DropDownList3.Items.Add(i5);
                DropDownList3.Items.Add(i6);
                DropDownList3.Items.Add(i7);
                DropDownList3.Items.Add(i8);
                DropDownList3.Items.Add(i9);
                DropDownList3.Items.Add(i10);
                DropDownList3.Items.Add(i11);
                DropDownList3.Items.Add(i12);
                DropDownList3.Items.Add(i13);
                DropDownList3.Items.Add(i14);
                DropDownList3.Items.Add(i15);

                PW.location_general = "100,101,102,103,104,105,106,107,108,129,130";
                DropDownList3.Visible = false;

                PW.location_id = Convert.ToInt32(DropDownList2.SelectedItem.Value);
                PW.Reports = "1";
                //DropDownList2.Items.FindByValue("Toronto Valet").Selected = true;

                Panel2.Visible = false; 

                DropDownList1.Items.Add(new ListItem("Web Rates","1"));
                DropDownList1.Items.Add(new ListItem("Paper Coupons Rates","2"));
                DropDownList1.Items.Add(new ListItem("Coupons Check","3"));

                DropDownList1.Items.Add(new ListItem("Web Rates at a Glance","4"));
                //if (Convert.ToInt32(Session["Level"].ToString()) < 3)
                if (Convert.ToInt32(Session["Level"].ToString()) < 3)
                {
                    DropDownList1.Items.Add(new ListItem("Ticket Sales","5")); 
                    DropDownList1.Items.Add(new ListItem("Ticket Revenue","6")); 
                  
                }
                DropDownList1.Items.Add(new ListItem("Expiring Web Rates","7"));
                if (Convert.ToInt32(Session["Level"].ToString()) < 4)
                {
                    DropDownList1.Items.Add(new ListItem("Web Discount Investigation", "8"));
                }
                DropDownList1.Items.Add(new ListItem("Revenue for Coupons Expiring","9"));
                if (Convert.ToInt32(Session["Level"].ToString()) < 3)
                {
                    DropDownList1.Items.Add(new ListItem("Occurrences and Tickets", "10"));
                }
                DropDownList1.Items.Add(new ListItem("PromoRequest Setup","11"));
                DropDownList1.DataBind();

                TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                TextBox5.Text = "01/01/2099";

            }

            if (Session["indA"].ToString() != "1")
            {

                string close = @"<script type='text/javascript'>
                                window.returnValue = true; 
                                window.close();
                                </script>";
                base.Response.Write(close);

            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (PW.indB == 0)
            {
                DropDownList1.Enabled = false;

                switch (PW.Reports)

                //if (PW.Reports == "1")
                {
                    case "1":
                        DateTime dt5 = Convert.ToDateTime(TextBox4.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                        DateTime dt51 = Convert.ToDateTime(TextBox5.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                       // daWC.Fill(dtWC, PW.location_id, Convert.ToInt32(TextBox1.Text), dt5, dt51);
                        daWC.Fill(dtWC, Convert.ToInt32(DropDownList2.SelectedItem.Value), Convert.ToInt32(TextBox1.Text), dt5, dt51);

                        ReportViewer1.Visible = true;
                        ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                        ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                        ReportViewer1.LocalReport.Refresh();

                        //ReportViewer1.LocalReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\pnfw2\pnfw1\Report1.rdlc";
                        ReportViewer1.LocalReport.ReportPath = @"Report11.rdlc";


                        Microsoft.Reporting.WebForms.ReportDataSource dsWCoupons = new Microsoft.Reporting.WebForms.ReportDataSource();
                        dsWCoupons.Value = dtWC;
                        dsWCoupons.Name = "dsWCoupons";
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportViewer1.LocalReport.DataSources.Add(dsWCoupons);
                        ReportViewer1.LocalReport.Refresh();
                        PW.indB = 1;
                        Button1.Text = "Back";
                        Panel1.Visible = false;
                    break;

                    case "2":
                    DateTime dt6 = Convert.ToDateTime(TextBox4.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    daPC.Fill(dtPC, PW.location_id, Convert.ToInt32(TextBox1.Text), dt6);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    //ReportViewer1.LocalReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\pnfw2\pnfw1\Report1.rdlc";
                    ReportViewer1.LocalReport.ReportPath = @"ReportPaperCoupons.rdlc";


                    Microsoft.Reporting.WebForms.ReportDataSource dsPCoupons = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsPCoupons.Value = dtPC;
                    dsPCoupons.Name = "dsWCoupons";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsPCoupons);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    break;

                   case "3":
                    //////DateTime dt6 = Convert.ToDateTime(TextBox4.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    daCC.Fill(dtCC, Convert.ToInt64(TextBox1.Text));
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.LocalReport.ReportPath = @"ReportPaperCoupons.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsCCoupons = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsCCoupons.Value = dtCC;
                    dsCCoupons.Name = "dsWCoupons";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsCCoupons);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    break;

                    case "4":
                    DateTime dt14 = Convert.ToDateTime(TextBox2.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    daWRG.Fill(dtWRG, dt14);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    //ReportViewer1.LocalReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\pnfw2\pnfw1\Report1.rdlc";
                    ReportViewer1.LocalReport.ReportPath = @"ReportWebGlance.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsWRG = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsWRG.Value = dtWRG;
                    dsWRG.Name = "dsWRGR";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsWRG);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    break;

                    case "5":

                    DateTime dt1 = Convert.ToDateTime(TextBox2.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    DateTime dt2 = Convert.ToDateTime(TextBox3.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);

                    daTS.Fill(dtTS, (string)Session["Locations"], dt1, dt2);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();
                    ReportViewer1.LocalReport.ReportPath = @"ReportTicketsSales.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsTSales = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsTSales.Value = dtTS;
                    dsTSales.Name = "dsTicketsSalesR";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsTSales);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    Panel2.Visible = false;


                    break;

                    case "6":

                    DateTime dt3 = Convert.ToDateTime(TextBox2.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    DateTime dt4 = Convert.ToDateTime(TextBox3.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    daTR.Fill(dtTR, (string)Session["Locations"], dt3, dt4);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();
                    ReportViewer1.LocalReport.ReportPath = @"ReportTicketsRevenue.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsTRevenue = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsTRevenue.Value = dtTR;
                    dsTRevenue.Name = "dsTicketsSalesR";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsTRevenue);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    break;

                    case "7":

                    DateTime dtW1 = Convert.ToDateTime(TextBox2.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    DateTime dtW2 = Convert.ToDateTime(TextBox3.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    daEWD.Fill(dtEWD, (string)Session["Locations"], dtW1, dtW2);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    var names = (from DataRow dr in dtEWD.Rows
                                 select dr["discount_id"]).Distinct();
                    var result = string.Join(",", names);

                    ReportViewer1.LocalReport.ReportPath = @"ReportExpiringWebDiscounts.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsEWD = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsEWD.Value = dtEWD;
                    dsEWD.Name = "dsExpiringWDiscounts";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsEWD);

                    Microsoft.Reporting.WebForms.ReportParameter[] param = new Microsoft.Reporting.WebForms.ReportParameter[2];                    
                    param[0] = new Microsoft.Reporting.WebForms.ReportParameter("StartDate",dtW1.ToString("MMM d, yyyy"));
                    param[1] = new Microsoft.Reporting.WebForms.ReportParameter("EndDate", dtW2.ToString("MMM d, yyyy"));
                    ReportViewer1.LocalReport.SetParameters(param);

                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    break;

                    case "8":
                    daWEDR.Fill(dtWEDR, Convert.ToInt32(TextBox1.Text), (string)Session["Locations"]);
                    daWEDCGR.Fill(dtWEDCGR, Convert.ToInt32(TextBox1.Text), DateTime.Now.Date.AddYears(-1), DateTime.Now.Date, (string)Session["Locations"]);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.LocalReport.ReportPath = @"ReportExpiringWebDiscountsCoupons.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsWEDRD = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsWEDRD.Value = dtWEDR;
                    dsWEDRD.Name = "dsWEDRatesR";

                    Microsoft.Reporting.WebForms.ReportDataSource dsWEDCGRD = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsWEDCGRD.Value = dtWEDCGR;
                    dsWEDCGRD.Name = "dsWEDCouponGroupsRevenueR";

                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsWEDRD);
                    ReportViewer1.LocalReport.DataSources.Add(dsWEDCGRD);

                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;

                    break;

                    case "9":

                    DateTime dtW9 = Convert.ToDateTime(TextBox2.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    DateTime dtW90 = Convert.ToDateTime(TextBox3.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    daECR.Fill(dtECR, dtW9, dtW90);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    //var names9 = (from DataRow dr in dtEWD.Rows
                    //             select dr["discount_id"]).Distinct();
                    //var result9 = string.Join(",", names);

                    ReportViewer1.LocalReport.ReportPath = @"ReportRevenueCouponsExpiring.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsECR = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsECR.Value = dtECR;
                    dsECR.Name = "dsECRevenue";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsECR);

                    Microsoft.Reporting.WebForms.ReportParameter[] param9 = new Microsoft.Reporting.WebForms.ReportParameter[2];
                    param9[0] = new Microsoft.Reporting.WebForms.ReportParameter("StartDate", dtW9.ToString("MMM d, yyyy"));
                    param9[1] = new Microsoft.Reporting.WebForms.ReportParameter("EndDate", dtW90.ToString("MMM d, yyyy"));
                    ReportViewer1.LocalReport.SetParameters(param9);

                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    break;

                    case "10":
                    //////DateTime dt6 = Convert.ToDateTime(TextBox4.Text, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
                    //daCC.Fill(dtCC, Convert.ToInt64(TextBox1.Text));
                    daOT.Fill(dtOT, PW.location_general);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.LocalReport.ReportPath = @"ReportOccurTickets.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsrOccurTickets = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsrOccurTickets.Value = dtOT;
                    dsrOccurTickets.Name = "dsrOccurTickets";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsrOccurTickets);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    DropDownList3.Visible = false;
                    break;

                //}
                //else
                //{
                    default:
                    daRR.Fill(dtRR, TextBox1.Text, PW.location_id, DateTime.Now);
                    ReportViewer1.Visible = true;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.Refresh();

                    //ReportViewer1.LocalReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\pnfw2\pnfw1\Report1.rdlc";
                    ReportViewer1.LocalReport.ReportPath = @"PromoRequest.rdlc";

                    Microsoft.Reporting.WebForms.ReportDataSource dsRRatesS = new Microsoft.Reporting.WebForms.ReportDataSource();
                    dsRRatesS.Value = dtRR;
                    dsRRatesS.Name = "dsRRate";
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(dsRRatesS);
                    ReportViewer1.LocalReport.Refresh();
                    PW.indB = 1;
                    Button1.Text = "Back";
                    Panel1.Visible = false;
                    break;
                }

            }
            else
            {
                DropDownList1.Enabled = true;

                ReportViewer1.Visible = false;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.Refresh();

                Button1.Text = "Run";
                PW.indB = 0;
                switch (PW.Reports)
                {
                    case "1":
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = true;
                        TextBox5.Visible = true;
                        Label6.Visible = true;
                        Label7.Visible = true;
                        img3.Visible = true;
                        img4.Visible = true;
                        TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        TextBox5.Text = "01/01/2099";
                        Label3.Text = "Referral id";
                        DropDownList2.Visible = true;
                        Label2.Visible = true;
                        TextBox1.Visible = true;
                        DropDownList3.Visible = false;
                        break;
                   case "2":
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = true;
                        Label6.Visible = true;
                        img3.Visible = true;
                        TextBox5.Visible = false;
                        Label7.Visible = false;
                        img4.Visible = false;

                        TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        Label3.Text = "Coupon Group id";
                        DropDownList2.Visible = true;
                        Label2.Visible = true;
                        TextBox1.Visible = true;
                        DropDownList3.Visible = false;
                        break;
                   case "3":
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = false;
                        Label6.Visible = false;
                        img3.Visible = false;
                        TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        Label3.Text = "Coupon";
                        DropDownList2.Visible = false;
                        TextBox5.Visible = false;
                        Label7.Visible = false;
                        img4.Visible = false;
                        Label2.Visible = false;
                        TextBox1.Visible = true;
                        DropDownList3.Visible = false;
                        break;
                    case "4":
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        img2.Visible = false;
                        TextBox3.Visible = false;
                        Label5.Visible = false;
                        TextBox2.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        Label4.Text = "Park Date";
                     
                        break;
                    case "5":
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        img2.Visible = true;
                        TextBox3.Visible = true;
                        Label5.Visible = true;
                        Label4.Text = "Open Date";
                   
                        break;
                    case "6":
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        img2.Visible = true;
                        TextBox3.Visible = true;
                        Label5.Visible = true;
                        Label4.Text = "Open Date";
                
                        break;
                    case "7":
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        img2.Visible = true;
                        TextBox3.Visible = true;
                        Label5.Visible = true;
                        Label4.Text = "Open Date";
             
                        break;
                    case "8":
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = false;
                        Label6.Visible = false;
                        img3.Visible = false;
                        TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        Label3.Text = "Discount ID";
                        DropDownList2.Visible = false;
                        TextBox5.Visible = false;
                        Label7.Visible = false;
                        img4.Visible = false;
                        Label2.Visible = false;
                        TextBox1.Visible = true;
                        DropDownList3.Visible = false;
                        break;
                    case "9":
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        img2.Visible = true;
                        TextBox3.Visible = true;
                        Label5.Visible = true;
                        Label4.Text = "Open Date";

                        break;
                    case "10":
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = false;
                        Label6.Visible = false;
                        img3.Visible = false;
                        //TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        Label3.Text = "";
                        DropDownList2.Visible = false;
                        Label2.Visible = true;
                        TextBox1.Visible = false;;
                        DropDownList3.Visible = true;
                        TextBox5.Visible = false;
                        Label7.Visible = false;
                        img4.Visible = false;

                        break;
                    default:
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = false;
                        img3.Visible = false;
                        //TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        TextBox1.Visible = true;
                        DropDownList2.Visible = true;
                        Label2.Visible = true;
                        Label6.Visible = false;
                        DropDownList3.Visible = false;
                        TextBox5.Visible = false;
                        Label7.Visible = false;
                        img4.Visible = false;
                        Label3.Text = "Referral ID";
                        TextBox1.Text = "3189";

                        break;
                }
            }

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            PW.location_id = Convert.ToInt32(DropDownList2.SelectedItem.Value);
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PW.Reports = DropDownList1.SelectedItem.Value;

            switch (PW.Reports)
            {
                case "1":
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                    TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox4.Visible = true;
                    Label6.Visible = true;
                    img3.Visible = true;
                    TextBox5.Visible = true;
                    Label7.Visible = true;
                    img4.Visible = true;
                    TextBox5.Text = "01/01/2099";
                    Label3.Text = "Referral id";
                    TextBox1.Text = "3189";
                    DropDownList2.Visible = true;
                    Label2.Visible = true;
                    TextBox1.Visible = true;
                    DropDownList3.Visible = false;
                    break;
                case "2":
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                    TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox4.Visible = true;
                    Label6.Visible = true;
                    img3.Visible = true;
                    Label3.Text = "Coupon Group id";
                    TextBox1.Text = "";
                    DropDownList2.Visible = true;
                    TextBox4.Visible = true;
                    Label2.Visible = true;
                    TextBox1.Visible = true;
                    DropDownList3.Visible = false;
                    TextBox5.Visible = false;
                    Label7.Visible = false;
                    img4.Visible = false;

                    break;
                case "3":
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                    TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox4.Visible = false;
                    Label6.Visible = false;
                    img3.Visible = false;
                    Label3.Text = "Coupon";
                    TextBox1.Text = "";
                    DropDownList2.Visible = false;
                    TextBox4.Visible = false;
                    Label2.Visible = false;
                    TextBox1.Visible = true;
                    TextBox1.Focus();
                    DropDownList3.Visible = false;
                    TextBox5.Visible = false;
                    Label7.Visible = false;
                    img4.Visible = false;

                    break;
                case "4":
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    img2.Visible = false;
                    TextBox3.Visible = false;
                    Label5.Visible = false;
                    TextBox2.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    Label4.Text = "Park Date";

                break;
                case "5":
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    TextBox2.Text = "01/01/" + DateTime.Now.Year.ToString();
                    TextBox3.Text = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                    img2.Visible = true;
                    TextBox3.Visible = true;
                    Label5.Visible = true;
                    Label4.Text = "Open Date";

                break;
                case "6":
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    TextBox2.Text = "01/01/" + DateTime.Now.Year.ToString();
                    TextBox3.Text = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                    img2.Visible = true;
                    TextBox3.Visible = true;
                    Label5.Visible = true;
                    Label4.Text = "Open Date";
                break;
                case "7":
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    TextBox2.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox3.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    img2.Visible = true;
                    TextBox3.Visible = true;
                    Label5.Visible = true;
                    Label4.Text = "Open Date";
                break;
                case "8":
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                    TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox4.Visible = false;
                    Label6.Visible = false;
                    img3.Visible = false;
                    Label3.Text = "Discount ID";
                    TextBox1.Text = "";
                    DropDownList2.Visible = false;
                    TextBox4.Visible = false;
                    Label2.Visible = false;
                    TextBox1.Visible = true;
                    DropDownList3.Visible = false;
                    TextBox1.Focus();
                    TextBox5.Visible = false;
                    Label7.Visible = false;
                    img4.Visible = false;
                break;
                case "9":
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    TextBox2.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox3.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    img2.Visible = true;
                    TextBox3.Visible = true;
                    Label5.Visible = true;
                    Label4.Text = "Open Date";
                break;
                case "10":
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                    //TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    TextBox4.Visible = false;
                    Label6.Visible = false;
                    img3.Visible = false;
                    Label3.Text = "";
                    TextBox1.Text = "";
                    DropDownList2.Visible = true;
                    Label2.Visible = true;
                    TextBox1.Visible = false;
                    DropDownList3.Visible = true;
                    DropDownList2.Visible = false;
                    TextBox5.Visible = false;
                    Label7.Visible = false;
                    img4.Visible = false;

                break;

                default:
                          Panel1.Visible = true;
                        Panel2.Visible = false;
                        TextBox4.Visible = false;
                        Label6.Visible = true;
                        img3.Visible = false;
                        //TextBox4.Text = DateTime.Now.ToString("MM/dd/yyyy");
                        TextBox1.Visible = true;
                        DropDownList2.Visible = true;
                        Label2.Visible = true;
                        Label6.Visible = false;
                        TextBox5.Visible = false;
                        Label7.Visible = false;
                        img4.Visible = false;
                        Label3.Text = "Referral ID";
                        TextBox1.Text = "3189";
                break;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Navigator.aspx");
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            PW.location_general = DropDownList3.SelectedItem.Value;
        }
    }
}