﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewRatesRequest.aspx.cs" Inherits="pnfw1.NewRatesRequest" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StylesNewRatesRequest.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="NEW RATES REQUEST"></asp:Label>

    
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />

    
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"  Visible ="false"
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1005px" Height="751px" style="margin-right: 0px">
                <LocalReport ReportEmbeddedResource="pnfw1.Report1.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
            </rsweb:ReportViewer>
    
    </div>
    </form>
</body>
</html>
