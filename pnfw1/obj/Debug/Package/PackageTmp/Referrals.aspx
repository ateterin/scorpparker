﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Referrals.aspx.cs" Inherits="pnfw1.Referrals" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
    <link href="StyleReferrals.css" rel="stylesheet" />
    <link href="calendar-blue.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-en.min.js" type="text/javascript"></script>
    
     <script type="text/javascript">
    var jQuery_1_4_1 = $.noConflict(true);
    </script>


    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

   <%-- <script type="text/javascript">
    var jQuery_3_5_1 = $.noConflict(true);
    </script>--%>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 8pt;
        }
    </style>

        <script type="text/javascript">

                jQuery_1_4_1 (document).ready(function () {
            jQuery_1_4_1 ("#<%=txtOpenDate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%m/%d/%Y",
                //ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
        jQuery_1_4_1 (document).ready(function () {
            jQuery_1_4_1 ("#<%=txtCloseDate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%m/%d/%Y",
                //ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
    </script>

    <script type="text/javascript">
        function Load() {
            <%--var user = "<%= Session["Locations"] %>";
            alert("user:" + user);--%>
            localStorage.setItem("refId", "0");
            localStorage.setItem("SC", "S");
            localStorage.setItem("CC", "N");
            localStorage.setItem("error", "N");
            document.getElementById("txtRfrrlId").value = "";            

            window.location.reload(false); 
        }

        function Load1() {
            
            localStorage.setItem("refId", "10000000");
            localStorage.setItem("SC", "C");
            localStorage.setItem("CC", "N");
            localStorage.setItem("error", "N");

            window.location.reload(false);
        }

        function Load2() {

            window.location.reload(false);
        }


        $(function () {

            document.getElementById("Button1").style.display = 'none';
            document.getElementById("btnNewRef").style.display = 'none';
            document.getElementById('lblWarningDates').style.display = 'none';


            //alert("REF:" + localStorage.getItem("refId"));
            var rfl = document.getElementById("txtRfrrlId").value;

            if (localStorage.getItem("refId") == "0") {
                document.getElementById("Panel2").style.display = 'none';
            }

            if (localStorage.getItem("refId") == "0") {
                $("#DIV5").css("visibility", "hidden")
                $("#DIV4").css("visibility", "hidden")
            }


            //alert("local: " + localStorage.getItem("refId"));

            if ( rfl!="") {
                localStorage.setItem("refId", rfl);
            }

            $.ajax({
                type: "POST",
                url: "Referrals.aspx/GetReferrals",
                data: '{rID:' + localStorage.getItem("refId") + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        });

        document.getElementById("Button1").style.display = '';
        document.getElementById("btnNewRef").style.display = '';
            //Hide();
           // alert("WORK1");
        //};
        function OnSuccess(response) {

           // alert("refId:" + localStorage.getItem("refId") + "/SC:" + localStorage.getItem("SC") + "/CC:" + localStorage.getItem("CC"));

            document.getElementById("Button1").style.display = '';
            document.getElementById("btnNewRef").style.display = '';

            var events = $('#events');
            $("[id*=GridView1]").DataTable(
            {
                    //  dom: 'Bfrtip',
                bLengthChange: true,
                lengthMenu: [[1000, -1], [1000, "All"]],
                bFilter: true,
                bSort: true,
                bPaginate: true,
                    select: true,  
                data: response.d,
                    columns: [{ 'data': 'referral_id' },
                        { 'data': 'master_id' },
                        { 'data': 'segment_code' },
                        { 'data': 'referral_code' },
                        { 'data': 'iata' },
                        { 'data': 'name' },
                        { 'data': 'commission_program_id' },
                        { 'data': 'sales_person' },
                        { 'data': 'open_date' },
                        { 'data': 'close_date' },
                        { 'data': 'address_id' },
                        { 'data': 'market_code' },
                        { 'data': 'commission_split_perc' },
                        { 'data': 'contact' },
                        { 'data': 'allow_best_rate' }]
            });
           // alert("111");
            //var data = table.row( 1 ).data();
                var table = $('#GridView1').DataTable();
                if (localStorage.getItem("refId") != "0") {
                    table.row(':eq(0)', { page: 'current' }).select();
                }
            //table.row(':eq(0)', { page: 'current' }).select();
           // var idd = table.row( 0 ).data().pluck('referral_id').toArray();
                var refid = document.getElementById("txtReferralId");
            //    var name = document.getElementById("txtName");
           //alert("1" + localStorage.getItem("refId"));
                refid.value = localStorage.getItem("refId");

            //alert("refId:" + refId.value);

            //document.querySelector('#btnSave').innerText = 'Hi
            //document.getElementById("btnSave").innerHTML = "Hi";
            //document.getElementById('<%=btnSave.ClientID %>').value= "Hide";

            var btnS = document.getElementById("<%= btnSave.ClientID %>");
            btnS.value = "Save";


            if (localStorage.getItem("SC") == "C") {
                $("#DIV4").css("visibility", "hidden");

                btnS.value = 'Create';

                var name = document.getElementById("txtName");
                var opendate = document.getElementById("txtOpenDate");
                name.value = "name?";

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                today = mm + '/' + dd + '/' + yyyy;
                opendate.value = today;
            }
            else {
                btnS.value = 'Save';
                var ccreate = "<%= Session["CCreate"] %>";
                if (ccreate == "Y") {
                    $("#DIV4").css("visibility", "visible")
                }
                else {
                    $("#DIV4").css("visibility", "hidden")
                }
            }
           
            if (localStorage.getItem("refId") == "0") {
                var refcode = document.getElementById("txtRefCode");
                var iata = document.getElementById("txtIata");
                var markcode = document.getElementById("txtMarkCode");
                var name = document.getElementById("txtName");
                var splitperc = document.getElementById("txtSplitPerc");
                var opendate = document.getElementById("txtOpenDate");
                var closedate = document.getElementById("txtCloseDate");
                var contact = document.getElementById("txtContact");
                
                $("#txtAddressId").val("");
                $("#txtStreet").val("");
                $("#txtNumber").val("");
                $("#txtApt").val("");
                $("#txtCity").val("");
                $("#txtProv").val("");
                $("#txtCountry").val("");
                $("#txtPCode").val("");
                $("#txtPhone").val("");
                $("#txtFax").val("");
                $("#txtSite").val("");
                
                //document.getElementById('ddlLocation').value = 100;
                //name.value = "";
                refcode.value = "";
                iata.value = "";
                markcode.value = "";
                document.getElementById('ddlSegmCode').value = "1";
                name.value = "name?";
                document.getElementById('ddlMaster').value = "0";
                document.getElementById('ddlProgram').value = "0";
                splitperc.value = "";
               // alert("222");
                var today = new Date();
                var dd = today.getDate();

                var mm = today.getMonth()+1; 
                var yyyy = today.getFullYear();
                if(dd<10) 
                {
                    dd='0'+dd;
                } 

                if(mm<10) 
                {
                    mm='0'+mm;
                } 

                today = mm+'/'+dd+'/'+yyyy;

                opendate.value = today;
                closedate.value = "";
                document.getElementById('ddlSalesPerson').value = "0";
                contact.value = "";

                document.getElementById('lblTitle1').innerHTML = 'Referral ID Details';
                document.getElementById('lblTitle2').innerHTML = 'Address ID ';

                document.getElementById("Panel2").style.display = 'none';
                //document.getElementById("DIV3").style.display = 'none';
                $("#DIV5").css("visibility", "hidden")
                $("#DIV4").css("visibility", "hidden")


                <%--var btnS = document.getElementById("<%= btnSave.ClientID %>");
                btnS.value = "Save";--%>
            }

            if (localStorage.getItem("refId") != "0"&& localStorage.getItem("refId") != "10000000" ) {
                var table = $('#GridView1').DataTable();

                var ids = table.rows(0).data().pluck('referral_id').toArray();
                var iatas = table.rows(0).data().pluck('iata').toArray();
                var refcodes = table.rows(0).data().pluck('referral_code').toArray();
                var markcodes = table.rows(0).data().pluck('market_code').toArray();
                var names = table.rows(0).data().pluck('name').toArray();
                var segmcodes = table.rows(0).data().pluck('segment_code').toArray();
                var masters = table.rows(0).data().pluck('master_id').toArray();
                var programs = table.rows(0).data().pluck('commission_program_id').toArray();
                var splitpercs = table.rows(0).data().pluck('commission_split_perc').toArray();
                var opendates = table.rows(0).data().pluck('open_date').toArray();
                var closedates = table.rows(0).data().pluck('close_date').toArray();
                var salespersons = table.rows(0).data().pluck('sales_person').toArray();
                var contacts = table.rows(0).data().pluck('contact').toArray();

                var addressids = table.rows(0).data().pluck('address_id').toArray();

                var refid = document.getElementById("txtReferralId");
                var refcode = document.getElementById("txtRefCode");
                var iata = document.getElementById("txtIata");
                var markcode = document.getElementById("txtMarkCode");
                var name = document.getElementById("txtName");
                var splitperc = document.getElementById("txtSplitPerc");
                var opendate = document.getElementById("txtOpenDate");
                var closedate = document.getElementById("txtCloseDate");
                var contact = document.getElementById("txtContact");
                
                //alert("ref not 0");
                refcode.value = refcodes;
                iata.value = iatas;
                markcode.value = markcodes;
                document.getElementById('ddlSegmCode').value = segmcodes;
                name.value = names;
                document.getElementById('ddlMaster').value = masters;
                document.getElementById('ddlProgram').value = programs;
                splitperc.value = splitpercs;
                opendate.value = opendates;
                closedate.value = closedates;
                document.getElementById('ddlSalesPerson').value = salespersons;
                contact.value = contacts;
               
                document.getElementById('lblTitle1').innerHTML = 'Referral ID ' + ids + ' Details';
                document.getElementById('lblTitle3').style.display = '';
                document.getElementById('lblTitle3').innerHTML = 'Create Coupon for Referral ID' + ids;

                document.getElementById("Panel2").style.display = '';

                var rupdate4 = "<%= Session["RUpdate"] %>";
                var ccreate4 = "<%= Session["CCreate"] %>";

                if (rupdate4 == "Y") {
                    $("#DIV5").css("visibility", "visible");
                }
                else {
                    $("#DIV5").css("visibility", "hidden");
                }

                if (ccreate4 == "Y") {
                    $("#DIV4").css("visibility", "visible");
                }
                else {
                    $("#DIV4").css("visibility", "hidden");
                }

              
                var params = '{ID:' + addressids + '}';

                    $.ajax({
                        type: "POST",
                        url: "Referrals.aspx/GetData",
                        contentType: "application/json; charset=utf-8",
                       // data: JSON.stringify(obj),
                       //data: JSON.stringify(arr),
                        data: params,
                        dataType: "json",
                        error: function (jqXHR, sStatus, sErrorThrown) {
                            alert('data:  ' + sErrorThrown);
                            alert('Get Data Error:  ' + sStatus);
                        },
                        success: function (data) {
                    
                            var address = data.d;
                            $("#txtAddressId").val(addressids);
                            $("#txtStreet").val(address.Address1);
                            $("#txtNumber").val(address.Address2);
                            $("#txtApt").val(address.Apt);
                            $("#txtCity").val(address.City);
                            $("#txtProv").val(address.Province);
                            $("#txtCountry").val(address.Country);
                            $("#txtPCode").val(address.PostalCode);
                            $("#txtPhone").val(address.Phone);
                            $("#txtFax").val(address.Fax);
                            $("#txtSite").val(address.Site);

                            document.getElementById('lblTitle2').innerHTML = 'Address ID ' + addressids;
                        }
                    });
            }

            //alert("Coupon" + localStorage.getItem("CC"));

            if (localStorage.getItem("CC") == "N") {
                document.getElementById('lblCouponResult').innerHTML = "";
            }
            else {
                document.getElementById('lblCouponResult').innerHTML = 'Coupon group(s) for Referral ID ' + localStorage.getItem("refId") + ' and Discount ID ' + localStorage.getItem("DID") + ' is (are) created';
            }

            var rupdate1 = "<%= Session["RUpdate"] %>";

            //alert(rupdate);
            if (rupdate1 == "Y") {
                $("#DIV5").css("visibility", "visible");
            }
            else {
                $("#DIV5").css("visibility", "hidden");
            }

            var ccreate1 = "<%= Session["CCreate"] %>";

            //alert(rupdate);
            if (ccreate1 == "Y" && localStorage.getItem("refId") != "10000000" ) {
                $("#DIV4").css("visibility", "visible");
            }
            else {
                $("#DIV4").css("visibility", "hidden");
            }


           // alert("End2");

            $('#GridView1 tbody').on('click', 'tr', function () {
               // alert("CLICK");
                localStorage.setItem("SC", "S");

               // alert("CLICK2");
                var table = $('#GridView1').DataTable();
                //var rowData = table.row(this).data();
                // ... do something with `rowData`
                var ids = table.rows(this).data().pluck('referral_id').toArray();
                var iatas = table.rows(this).data().pluck('iata').toArray();
                var refcodes = table.rows(this).data().pluck('referral_code').toArray();
                var markcodes = table.rows(this).data().pluck('market_code').toArray();
                var names = table.rows(this).data().pluck('name').toArray();
                var segmcodes = table.rows(this).data().pluck('segment_code').toArray();
                var masters = table.rows(this).data().pluck('master_id').toArray();
                var programs = table.rows(this).data().pluck('commission_program_id').toArray();
                var splitpercs = table.rows(this).data().pluck('commission_split_perc').toArray();
                var opendates = table.rows(this).data().pluck('open_date').toArray();
                var closedates = table.rows(this).data().pluck('close_date').toArray();
                var salespersons = table.rows(this).data().pluck('sales_person').toArray();
                var contacts = table.rows(this).data().pluck('contact').toArray();

                var addressids = table.rows(this).data().pluck('address_id').toArray();



                //alert("2" + ids);
                var rfid=  document.getElementById("txtRfrrlId");
                var refid = document.getElementById("txtReferralId");
                var refcode = document.getElementById("txtRefCode");
                var iata = document.getElementById("txtIata");
                var markcode = document.getElementById("txtMarkCode");
                var name = document.getElementById("txtName");
                var splitperc = document.getElementById("txtSplitPerc");
                var opendate = document.getElementById("txtOpenDate");
                var closedate = document.getElementById("txtCloseDate");
                var contact = document.getElementById("txtContact");
                

                //refid.value = ids;
                //alert("2" + localStorage.getItem("refId"));
                if (localStorage.getItem("refId") == "0") {
                    refid.value = ids;
                    rfid.value = "";
                }

                //alert(refid.value);
                refcode.value = refcodes;
                iata.value = iatas;
                markcode.value = markcodes;
                document.getElementById('ddlSegmCode').value = segmcodes;
                name.value = names;
                document.getElementById('ddlMaster').value = masters;
                document.getElementById('ddlProgram').value = programs;
                splitperc.value = splitpercs;
                opendate.value = opendates;
                closedate.value = closedates;
                document.getElementById('ddlSalesPerson').value = salespersons;
                contact.value = contacts;
               
 //alert("S2");
                localStorage.setItem("refId", ids);
         //alert("click:"+ localStorage.getItem("refId"));
                document.getElementById('lblTitle1').innerHTML = 'Referral ID ' + ids + ' Details';

                document.getElementById('lblTitle3').style.display = '';
//alert("S4");
                document.getElementById('lblTitle3').innerHTML = 'Create Coupon for Referral ID' + ids;

               // document.getElementById("Panel2").style.display = '';


                var rupdate5 = "<%= Session["RUpdate"] %>";
                var ccreate5 = "<%= Session["CCreate"] %>";


                if (rupdate5 == "Y") {
                    document.getElementById("Panel2").style.display = '';
                    $("#DIV5").css("visibility", "visible");
                }
                else {
                    $("#DIV5").css("visibility", "hidden");
                }

               // alert("DIV4" + ccreate5)

                if (ccreate5 == "Y") {
                    document.getElementById("Panel2").style.display = '';
                    $("#DIV4").css("visibility", "visible");
                }
                else {
                    alert("DIV4");
                    $("#DIV4").css("visibility", "hidden");
                }

//alert("S5");
               

                var params = '{ID:' + addressids + '}';

                    //var params = '{"100000103"}';
                    //var obj = {};
                    //obj.name = $.trim($("100000105").val());
                    //obj.age = $.trim($("[id*=txtAge]").val());

                    $.ajax({
                        type: "POST",
                        url: "Referrals.aspx/GetData",
                        contentType: "application/json; charset=utf-8",
                       // data: JSON.stringify(obj),
                       //data: JSON.stringify(arr),
                        data: params,
                        dataType: "json",
                        error: function (jqXHR, sStatus, sErrorThrown) {
                            alert('data:  ' + sErrorThrown);
                            alert('Get Data Error:  ' + sStatus);
                        },
                        success: function (data) {
                    
                            //$("#oTable").empty();
                            var address = data.d;
                            //alert(address.Address1);
                           //alert(oTable.Rows[0].address_1)
                            $("#txtAddressId").val(addressids);
                            $("#txtStreet").val(address.Address1);
                            $("#txtNumber").val(address.Address2);
                            $("#txtApt").val(address.Apt);
                            $("#txtCity").val(address.City);
                            $("#txtProv").val(address.Province);
                            $("#txtCountry").val(address.Country);
                            $("#txtPCode").val(address.PostalCode);
                            $("#txtPhone").val(address.Phone);
                            $("#txtFax").val(address.Fax);
                            $("#txtSite").val(address.Site);

                            document.getElementById('lblTitle2').innerHTML = 'Address ID ' + addressids;

                            //for (i = 0; i <= oTable.Rows.length - 1; i++) {
                            //    $("#oTable").append("<tr><td>" + oTable.Rows[i].address_id + "</td><td>" + oTable.Rows[i].address_1 + "</td></tr>");
                            //}
                        }
                    });

                var rupdate = "<%= Session["RUpdate"] %>";
                var ccreate = "<%= Session["CCreate"] %>";

                   //alert(rupdate);
                    if (rupdate == "Y") {
                        $("#DIV5").css("visibility", "visible");
                    }
                    else {
                        $("#DIV5").css("visibility", "hidden");
                    }

                if (ccreate == "Y") {
                    $("#DIV4").css("visibility", "visible");
                }
                else {
                    $("#DIV4").css("visibility", "hidden");
                }
                            
            });

            //alert("W:" + localStorage.getItem("errorD"));
            var warning2 = document.getElementById('lblWarningDates');
            if (localStorage.getItem("error") == "Y") {               
                warning2.innerHTML = localStorage.getItem("errorD");
                warning2.style.display = '';
            }
            else {
                warning2.style.display = 'none';
            }

        };

        function Hide() {
            $("#DIV3").css("visibility", "hidden"); 
        }    
        function Hide1() {
                  $("#DIV2").css("visibility", "hidden");
            $("#DIV3").css("visibility", "hidden"); 
        }     

        function SaveCreate() {

            var opendate = document.getElementById("txtOpenDate");
            var closedate = document.getElementById("txtCloseDate");


            if (isValidDate(opendate.value) && (isValidDate(closedate.value)||closedate.value=="")) 
            {

                localStorage.setItem("CC", "N");
                localStorage.setItem("error", "N");
                localStorage.setItem("errorD", "");

                //var refid = document.getElementById("txtReferralId");
                var master = document.getElementById('ddlMaster');
                var segm = document.getElementById('ddlSegmCode');
                var refcode = document.getElementById("txtRefCode");
                var iata = document.getElementById("txtIata");
                var markcode = document.getElementById("txtMarkCode");
                var name = document.getElementById("txtName");
                var prog = document.getElementById('ddlProgram');
                var spers = document.getElementById('ddlSalesPerson');
                var splitperc = document.getElementById("txtSplitPerc");
                var contact = document.getElementById("txtContact");

                var addrid = document.getElementById("txtAddressId");
                var street = document.getElementById("txtStreet");
                var numb = document.getElementById("txtNumber");
                var apt = document.getElementById("txtApt");
                var city = document.getElementById("txtCity");
                var prov = document.getElementById("txtProv");
                var country = document.getElementById("txtCountry");
                var pcode = document.getElementById("txtPCode");
                var phone = document.getElementById("txtPhone");
                var fax = document.getElementById("txtFax");
                var site = document.getElementById("txtSite");

                var loctn = document.getElementById('ddlLocation');

                var refid1 = (localStorage.getItem("refId").trim() != "") ? localStorage.getItem("refId") : "ZERO";
                var master1 = (master.value.trim() != "") ? master.value : "ZERO";
                var segm1 = (segm.value.trim() != "") ? segm.value : "ZERO";
                var refcode1 = (refcode.value.trim() != "") ? refcode.value : "ZERO";
                var iata1 = (iata.value.trim() != "") ? iata.value : "ZERO";
                var markcode1 = (markcode.value.trim() != "") ? markcode.value : "ZERO";
                var name1 = (name.value.trim() != "") ? name.value : "ZERO";
                var prog1 = (prog.value.trim() != "") ? prog.value : "ZERO";
                var spers1 = (spers.value.trim() != "") ? spers.value : "ZERO";
                var splitperc1 = (splitperc.value.trim() != "") ? splitperc.value : "ZERO";
                var opendate1 = (opendate.value.trim() != "") ? opendate.value : "ZERO";
                var closedate1 = (closedate.value.trim() != "") ? closedate.value : "ZERO";
                var contact1 = (contact.value.trim() != "") ? contact.value : "ZERO";

                var addrid1 = (addrid.value.trim() != "") ? addrid.value : "ZERO";
                var street1 = (street.value.trim() != "") ? street.value : "ZERO";
                var numb1 = (numb.value.trim() != "") ? numb.value : "ZERO";
                var apt1 = (apt.value.trim() != "") ? apt.value : "ZERO";
                var city1 = (city.value.trim() != "") ? city.value : "ZERO";
                var prov1 = (prov.value.trim() != "") ? prov.value : "ZERO";
                var country1 = (country.value.trim() != "") ? country.value : "ZERO";
                var pcode1 = (pcode.value.trim() != "") ? pcode.value : "ZERO";
                var phone1 = (phone.value.trim() != "") ? phone.value : "ZERO";
                var fax1 = (fax.value.trim() != "") ? fax.value : "ZERO";
                var site1 = (site.value.trim() != "") ? site.value : "ZERO";

                var loctn1 = loctn ? loctn.value : "ZERO";

                var params = '{"RI":"' + refid1 + '","MR":"' + master1 + '","SG":"' + segm1 + '","RC":"' + refcode1 + '","IA":"' + iata1 + '","MK":"' + markcode1 + '","NM":"' +
                    name1 + '","PR":"' + prog1 + '","SR":"' + spers1 + '","SP":"' + splitperc1 + '","OD":"' + opendate1 + '","CD":"' + closedate1 + '","CN":"' + contact1 +
                    '","AD":"' + addrid1 + '","ST":"' + street1 + '","NU":"' + numb1 + '","AP":"' + apt1 + '","CI":"' + city1 + '","PV":"' + prov1 + '","CO":"' + country1 +
                    '","PC":"' + pcode1 + '","PH":"' + phone1 + '","FA":"' + fax1 + '","SI":"' + site1 + '","LO":"' + loctn1 + '","SC":"' + localStorage.getItem("SC") + '"}';

                //alert("params: " + params);

                $.ajax({
                    type: "POST",
                    url: "Referrals.aspx/SaveCreateRefAdr",
                    contentType: "application/json; charset=utf-8",
                    // data: JSON.stringify(obj),
                    //data: JSON.stringify(arr),
                    data: params,
                    dataType: "json",
                    error: function (jqXHR, sStatus, sErrorThrown) {
                        alert('data:  ' + sErrorThrown);
                        alert('Get Data Error:  ' + sStatus);
                    },
                    success: function (data) {

                        //alert("resp: " + data.d)

                        if (data.d == "Error") {
                            localStorage.setItem("error", "Y");
                            localStorage.setItem("errorD", "Error, action was not completed");
                        }
                        else {
                            localStorage.setItem("refId", data.d);
                            localStorage.setItem("SC", "S");
                        }

                        ////$("#oTable").empty();
                        //var address = data.d;
                        ////alert(address.Address1);
                        ////alert(oTable.Rows[0].address_1)
                        //$("#txtAddressId").val(addressids);
                        //$("#txtStreet").val(address.Address1);
                        //$("#txtNumber").val(address.Address2);
                        //$("#txtApt").val(address.Apt);
                        //$("#txtCity").val(address.City);
                        //$("#txtProv").val(address.Province);
                        //$("#txtCountry").val(address.Country);
                        //$("#txtPCode").val(address.PostalCode);
                        //$("#txtPhone").val(address.Phone);
                        //$("#txtFax").val(address.Fax);
                        //$("#txtSite").val(address.Site);

                        //document.getElementById('lblTitle2').innerHTML = 'Address ID ' + addressids;

                        //for (i = 0; i <= oTable.Rows.length - 1; i++) {
                        //    $("#oTable").append("<tr><td>" + oTable.Rows[i].address_id + "</td><td>" + oTable.Rows[i].address_1 + "</td></tr>");
                        //}
                    }
                });
                window.location.reload(false);
            }
            else {
                var warning = document.getElementById('lblWarningDates');
                warning.innerHTML = 'Wrong date format';
                warning.style.display = '';
            }

        }



        function CreateCoupon() {

            localStorage.setItem("error", "N");
            var textVal=document.getElementById("txtDiscountId").value;
            if (!textVal.match(/\S/)) {
                //alert("Field is blank");
                var warning = document.getElementById('lblWarningDates');
                warning.innerHTML = 'Discount Id field is empty';
                warning.style.display = '';
            }
            else {

                var refid = localStorage.getItem("refId");
                //alert("CC:" + refid);
                //var refid = document.getElementById("txtReferralId");
                var did = document.getElementById("txtDiscountId");
                var tar = document.getElementById('ddlTARates');
                var nl = document.getElementById('ddlNatLocal');
                var ci = document.getElementById('ddlCityI');

                //var refid1 = (refid.value.trim() != "") ? refid.value : "ZERO";
                var did1 = (did.value.trim() != "") ? did.value : "ZERO";
                var tar1 = (tar.value.trim() != "") ? tar.value : "ZERO";
                var nl1 = (nl.value.trim() != "") ? nl.value : "ZERO";
                var ci1 = (ci.value.trim() != "") ? ci.value : "ZERO";

                var paramcs = '{"RI":"' + refid + '","DI":"' + did1 + '","TR":"' + tar1 + '","NL":"' + nl1 + '","CI":"' + ci1 + '"}';

                //alert("paramcs: " + paramcs);

                $.ajax({
                    type: "POST",
                    url: "Referrals.aspx/CreateCpn",
                    contentType: "application/json; charset=utf-8",
                    // data: JSON.stringify(obj),
                    //data: JSON.stringify(arr),
                    data: paramcs,
                    dataType: "json",
                    error: function (jqXHR, sStatus, sErrorThrown) {
                        alert('data:  ' + sErrorThrown);
                        alert('Get Data Error:  ' + sStatus);
                    },
                    success: function (data) {

                        //alert("resp: " + data.d + "/refid:" + refid);

                        localStorage.setItem("refId", refid);
                        localStorage.setItem("SC", "S");
                        localStorage.setItem("DID", did.value);
                        if (data.d == "0") {
                            localStorage.setItem("CC", "Y");
                            localStorage.setItem("error", "N");
                            localStorage.setItem("errorD", "");
                        }
                        else {
                            localStorage.setItem("CC", "N");
                            localStorage.setItem("error", "Y");
                            localStorage.setItem("errorD", "Error, coupon was not created");

                        }
                        //$("#lblCouponResult").css("visibility", "visible");
                        //document.getElementById("lblCouponResult").style.display = '';
                        //document.getElementById('lblCouponResult').innerHTML = 'Coupon group for Referral ID ' + RI + ' and Discount ID ' + DI + ' is created';


                    }
                });

                window.location.reload(false);
            }
        }

        function isValidDate(dateString)
        {
             //First check for the pattern
            if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
                return false;

            // Parse the date parts to integers
            var parts = dateString.split("/");
            var day = parseInt(parts[1], 10);
            var month = parseInt(parts[0], 10);
            var year = parseInt(parts[2], 10);

            //alert(day + "/" + month + "/" + year);

            // Check the ranges of month and year
            if(year < 1000 || year > 3000 || month == 0 || month > 12)
                return false;

            var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

            // Adjust for leap years
            if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                monthLength[1] = 29;

            // Check the range of the day
            return day > 0 && day <= monthLength[month - 1];

            //const regExp = /^(\d\d?)\/(\d\d?)\/(\d{4})$/;
            //let matches = dateStr.match(regExp);
            //let isValid = matches;
            //let maxDate = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
   
            //if (matches) {
            //    const month = parseInt(matches[1]);
            //    const date = parseInt(matches[2]);
            //    const year = parseInt(matches[3]);

            //    alert(month + "/" + date + "/" + year);
     
            //    isValid = month <= 12 && month > 0;
            //    isValid &= date <= maxDate[month] && date > 0;
     
            //    const leapYear = (year % 400 == 0)
            //    || (year % 4 == 0 && year % 100 != 0);
            //    isValid &= month != 2 || leapYear || date <= 28; 
            //}
   
            //return isValid

            
            
        };


    </script>


</head>
<body>
    <form id="form1" runat="server">
        <div id ="block1">
            <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
            <asp:Label ID="Label1" runat="server" Text="REFERRALS"></asp:Label>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Navigator.aspx">Navigator</asp:HyperLink>
           
            <asp:Button ID="btnNewRef" runat="server" OnClick="btnNewRef_Click" Text="New Referral"  OnClientClick="Load1()" />


            <asp:Label ID="lblError" runat="server" Text="Sorry, ERROR" ForeColor="#FF0000"></asp:Label>
            

            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Load"  OnClientClick="Load(); return false;" />

            
            <asp:Panel ID="Panel0" runat="server"  
            Height="20" Width="1020"  >
            </asp:Panel>

                <%--<div id="DIV2">--%>

            

            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" 
            Height="280" Width="1020" style="margin-bottom: 0px"  >

                <%--<asp:GridView ID="GridView1" runat="server" AllowPaging="True"  AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="referral_id"  OnSorting="GridView1_Sorting" 
                     PageSize="1000" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound = "OnRowDataBound" OnSelectedIndexChanged = "OnSelectedIndexChanged" BackColor="#96FFA0">
                    <columns>
                        <asp:BoundField DataField="referral_id" HeaderText="RefID" ReadOnly="True"  ItemStyle-Width="20" SortExpression="referral_id"/>
                        <asp:BoundField DataField="master_id" HeaderText="MasterID" ReadOnly="True"   ItemStyle-Width="30" SortExpression="master_id"/>
                        <asp:BoundField DataField="segment_code" HeaderText="SegmCode" ReadOnly="True" ItemStyle-Width="30" SortExpression="segment_code"/>
                        <asp:BoundField DataField="referral_code" HeaderText="RefCode" ReadOnly="True"  ItemStyle-Width="30" SortExpression="referral_code"/>
                        <asp:BoundField DataField="iata" HeaderText="Iata" ReadOnly="True" ItemStyle-Width="40" SortExpression="iata"/>
                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" ItemStyle-Width="300" HeaderStyle-Width="300" ItemStyle-Wrap="false" SortExpression="name"/>
                        <asp:BoundField DataField="commission_program_id" HeaderText="CommissionProgID" ReadOnly="True"  ItemStyle-Width="30" SortExpression="commission_program_id"/>
                        <asp:BoundField DataField="sales_person" HeaderText="SalesPerson" ReadOnly="True" ItemStyle-Width="40" SortExpression="sales_person"/>
                        <asp:BoundField DataField="open_date" dataformatstring="{0:MM/dd/yyyy}" HeaderText="OpenDate" ReadOnly="True" ItemStyle-Width="80" ItemStyle-Wrap="false" SortExpression="open_date"/>
                        <asp:BoundField DataField="close_date" dataformatstring="{0:MM/dd/yyyy}" HeaderText="CloseDate" ReadOnly="True" ItemStyle-Width="80" ItemStyle-Wrap="false" SortExpression="close_date" />
                        <asp:BoundField DataField="address_id" HeaderText="AddressID" ReadOnly="True" ItemStyle-Width="80" SortExpression="address_id"/>
                        <asp:BoundField DataField="market_code" HeaderText="MarketCode" ReadOnly="True" ItemStyle-Width="40" SortExpression="market_code"/>
                        <asp:BoundField DataField="commission_split_perc" HeaderText="CommSplit%" ReadOnly="True" ItemStyle-Width="40" SortExpression="commission_split_perc" />
                        <asp:BoundField DataField="contact" HeaderText="Contact" ReadOnly="True" ItemStyle-Width="100" ItemStyle-Wrap="false" SortExpression="contact" />
                    </columns>
                </asp:GridView>--%>

                    <asp:GridView ID="GridView1" runat="server" CssClass="display compact" AutoGenerateColumns="false">
                        <Columns>
                            <asp:BoundField DataField="referral_id" HeaderText="Referral Id" />
                            <asp:BoundField DataField="master_id" HeaderText="Master Id" />
                            <asp:BoundField DataField="segment_code" HeaderText="Segment Code" />
                            <asp:BoundField DataField="referral_code" HeaderText="Referral Code" />
                            <asp:BoundField DataField="iata" HeaderText="Iata" />
                            <asp:BoundField DataField="name" HeaderText="Name" />
                            <asp:BoundField DataField="commission_program_id" HeaderText="Program Id" />
                            <asp:BoundField DataField="sales_person" HeaderText="Sales Person" />
                            <asp:BoundField DataField="open_date" HeaderText="Open Date" />
                            <asp:BoundField DataField="close_date" HeaderText="Close Date" />
                            <asp:BoundField DataField="address_id" HeaderText="Address Id" />
                            <asp:BoundField DataField="market_code" HeaderText="Market Code" />
                            <asp:BoundField DataField="commission_split_perc" HeaderText="Comm Perc" />
                            <asp:BoundField DataField="contact" HeaderText="contact" />
                            <asp:BoundField DataField="allow_best_rate" HeaderText="Best Rate" />
                        </Columns>
                    </asp:GridView>
                
            </asp:Panel>
            <%--</div>--%>
                <asp:TextBox id="txtReferralId" Text="0" runat="server" BorderStyle="None" />
                <asp:TextBox id="txtAddressId" Text="" runat="server" BorderStyle="None" />
                <asp:TextBox id="txtRfrrlId" Text="" runat="server" BorderStyle="None" />

            </ContentTemplate>
            </asp:UpdatePanel>
            

            <div id="DIV3">

            <asp:Panel ID="Panel2" runat="server" Height="500px" Width="1020px" BorderStyle="Double" >
                <div id ="DIV5">

                <asp:Label ID="lblTitle1" runat="server" Text="Referral Details" ForeColor="#006600"></asp:Label>
                <asp:Label ID="lblRefCode" runat="server" Text="Referral Code" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtRefCode" runat="server"></asp:TextBox>
                <asp:Label ID="lblIata" runat="server" Text="IATA" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtIata" runat="server"></asp:TextBox>
                <asp:Label ID="lblMarkCode" runat="server" Text="Market Code" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtMarkCode" runat="server"></asp:TextBox>
                <asp:Label ID="lblName" runat="server" Text="Name" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:Label ID="lblSegmCode" runat="server" Text="Segment Code" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlSegmCode" runat="server" AppendDataBoundItems="true"/>
                <asp:Label ID="lblMaster" runat="server" Text="Master" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlMaster" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value="" />
                </asp:DropDownList>
                <asp:Label ID="lblProgram" runat="server" Text="Program" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlProgram" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value="" />
                </asp:DropDownList>
                <asp:Label ID="lblSplitPerc" runat="server" Text="Split Percent" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtSplitPerc" runat="server"></asp:TextBox>
                <asp:Label ID="lblOpenDate" runat="server" Text="Open Date"></asp:Label>
                <asp:TextBox ID="txtOpenDate" runat="server"></asp:TextBox>
                <asp:Image ID="img1" runat="server" ImageUrl="calender.png" />
                <asp:Label ID="lblCloseDate" runat="server" Text="Close Date"></asp:Label>
                <asp:TextBox ID="txtCloseDate" runat="server"></asp:TextBox>
                <asp:Image ID="img2" runat="server" ImageUrl="calender.png" />
                <asp:Label ID="lblSalesPerson" runat="server" Text="Sales Person" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlSalesPerson" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value="" />
                </asp:DropDownList>
                <asp:Label ID="lblContact" runat="server" Text="Contact" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>               

                <asp:Label ID="lblTitle2" runat="server" Text="Address" ForeColor="#006600"></asp:Label>
                <asp:Label ID="lblStreet" runat="server" Text="Street Name" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtStreet" runat="server"></asp:TextBox>
                <asp:Label ID="lblNumber" runat="server" Text="Number" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtNumber" runat="server"></asp:TextBox>
                <asp:Label ID="lblApt" runat="server" Text="Suit/Apt#" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtApt" runat="server"></asp:TextBox>
                <asp:Label ID="lblCity" runat="server" Text="City" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                <asp:Label ID="lblProv" runat="server" Text="Prov/State" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtProv" runat="server"></asp:TextBox>
                <asp:Label ID="lblCountry" runat="server" Text="Country" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                <asp:Label ID="lblPCode" runat="server" Text="Postal Code" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtPCode" runat="server"></asp:TextBox>
                <asp:Label ID="lblLocation" runat="server" Text="Location" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlLocation" runat="server"  AutoPostBack="False" AppendDataBoundItems="true"/>
                <asp:Label ID="lblPhone" runat="server" Text="Phone" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                <asp:Label ID="lblFax" runat="server" Text="Fax" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                <asp:Label ID="lblSite" runat="server" Text="Site" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtSite" runat="server"></asp:TextBox>

                <asp:Label ID="lblWarningDates" runat="server" Text="Date is not in correct format" ForeColor="#FF0000"></asp:Label>

               <%-- <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save"  OnClientClick="Load2()" ForeColor="#006600"/>--%>
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save"  OnClientClick="SaveCreate(); return false; " ForeColor="#006600"/>

                <%--<button type="button" name ="btnSave" onclick="SaveCreate()">Save</button>--%>
                </div>
                <div id="DIV4">

                <asp:Label ID="lblTitle3" runat="server" Text="Create coupon" ForeColor="#006600"></asp:Label>
                <asp:Label ID="lblDiscountId" runat="server" Text="Discount ID" ForeColor="#006600"></asp:Label>
                <asp:TextBox ID="txtDiscountId" runat="server"></asp:TextBox>
                <asp:Label ID="lblTARates" runat="server" Text="TA Rates" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlTARates" runat="server">
                <asp:ListItem Value="Y">Yes</asp:ListItem>
                <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="lblNatLocal" runat="server" Text="National/Local" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlNatLocal" runat="server" AutoPostBack="False">
                <asp:ListItem Value="N">N</asp:ListItem>
                <asp:ListItem Value="L">L</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="lblCityI" runat="server" Text="City" ForeColor="#006600"></asp:Label>
                <asp:DropDownList ID="ddlCityI" runat="server" AutoPostBack="False">
                </asp:DropDownList>

                <asp:Button ID="btnCreateCoupon" runat="server" OnClick="btnCreateCoupon_Click" Text="Create Coupon" ForeColor="#006600" OnClientClick="CreateCoupon(); return false;"  />

                <asp:Label ID="lblCouponResult" runat="server" Text="" ForeColor="#006600"></asp:Label>
                </div>
            </asp:Panel>

        </div>
        </div>


        
    </form>
</body>
</html>
