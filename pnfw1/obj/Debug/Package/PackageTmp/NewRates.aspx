﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewRates.aspx.cs" Inherits="pnfw1.NewRates" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
    <link href="StylesNewRates.css" rel="stylesheet" />
    <link href="calendar-blue.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-en.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=TextBox12.ClientID %>").dynDateTime({
            showsTime: true,
            ifFormat: "%m/%d/%Y",
            //ifFormat: "%Y/%m/%d %H:%M",
            daFormat: "%l;%M %p, %e %m,  %Y",
            align: "BR",
            electric: false,
            singleClick: false,
            displayArea: ".siblings('.dtcDisplayArea')",
            button: ".next()"
        });
        });
        $(document).ready(function () {
            $("#<%=TextBox13.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%m/%d/%Y",
                //ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });

        function CalcDiscount(Discount, Rate, RRate, sId, RType) {
            if (RType == "1") {
                var cRate = parseFloat(document.getElementById(Rate).value);
                //var cRRate = parseFloat(document.getElementById(RRate).value);
                //var vcDiscount = parseFloat(RRate - cRate).toFixed(2).replace(/\.?0+$/, "");
                //var vcDiscount = parseFloat(RRate - cRate).toFixed(2).replace(/\.?0+$/, "");
                var vcDiscountx = parseFloat(RRate - cRate);
                var ccDisc = Math.floor(vcDiscountx * 1000) / 1000;
                var vcDiscount = ccDisc.toFixed(2).replace(/\.?0+$/, "");
                //var ccD = Math.floor(vcDiscount * 100) / 100;
                var vcDiscPers = parseFloat(100 * (RRate - cRate) / RRate);
                var ccPx = Math.floor(vcDiscPers * 1000) / 1000;
                var ccP = ccPx.toFixed(2).replace(/\.?0+$/, "");
                var cDiscount = document.getElementById(Discount);
                if (isNaN(vcDiscount)) {
                    vcDiscount = "";
                }
                if (isNaN(ccP)) {
                    ccP = "";
                }
                cDiscount.innerHTML = vcDiscount + " (" + ccP + "%)";

                if (sId == "H") {
                    document.getElementById('HiddenField1').value = vcDiscount + " (" + ccP + "%)";
                }
                if (sId == "D") {
                    document.getElementById('HiddenField2').value = vcDiscount + " (" + ccP + "%)";
                }
                if (sId == "W") {
                    document.getElementById('HiddenField3').value = vcDiscount + " (" + ccP + "%)";
                }
                if (sId == "M") {
                    document.getElementById('HiddenField4').value = vcDiscount + " (" + ccP + "%)";
                }
            }


        }
    </script>
<%--    <script type="text/javascript">
        function CalcSellPrice2(CurrentPrice, QuntValue, TotalValue) {
            var QuntVar = parseFloat(document.getElementById(QuntValue).value);
            var TotalVar = document.getElementById(TotalValue);
            var TotalPriceValue = parseFloat(CurrentPrice * QuntVar);
            TotalVar.innerHTML = TotalPriceValue;
        }
    </script>--%>


</head>
<body>
    <form id="form1" runat="server">
    <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="NEW RATES REQUEST"></asp:Label>

        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Navigator.aspx">Navigator</asp:HyperLink>

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>


        <asp:Panel ID="Panel1" runat="server" Height="718px">
            <asp:Label ID="Label2" runat="server" Text="Location"></asp:Label>
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="Label32" runat="server" Text="100"></asp:Label>
            <asp:Label ID="Label3" runat="server" Text="Web/Paper"></asp:Label>
            <asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem Text="Web" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Paper" Value="2"></asp:ListItem>
            </asp:DropDownList>

            <asp:Label ID="Label4" runat="server" Text="Open Date"></asp:Label>
            <asp:TextBox ID="TextBox12" runat="server" AutoPostBack="True" OnTextChanged="TextBox12_TextChanged"></asp:TextBox>
            <img id="img1" src="calender.png" />
            <asp:Label ID="Label5" runat="server" Text="Close Date"></asp:Label>
            <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
            <img id="img2" src="calender.png" />
            <asp:Label ID="Label6" runat="server" Text="Discount Type"></asp:Label>
            <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                    <asp:ListItem Text="DOLL" Value="1"></asp:ListItem>
                    <asp:ListItem Text="FLAT" Value="2"></asp:ListItem>
                    <asp:ListItem Text="DAYS" Value="3"></asp:ListItem>

                    <%--<asp:ListItem Text="PERC" Value="2"></asp:ListItem>--%>
            </asp:DropDownList>
            <asp:Label ID="Label7" runat="server" Text="Referral Id"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

            <asp:Label ID="Label8" runat="server" Text="Day of Week Stayed"></asp:Label>
            <%--<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>--%>
            <div id ="DivFirst">
            <div id ="Div1"><asp:CheckBox ID="CheckBox1" runat="server" Text="Mo" /></div>
            <div id ="Div2"><asp:CheckBox ID="CheckBox2" runat="server" Text="Tu" /></div>
            <div id ="Div3"><asp:CheckBox ID="CheckBox3" runat="server" Text="We" /></div>
            <div id ="Div4"><asp:CheckBox ID="CheckBox4" runat="server" Text="Th" /></div>
            <div id ="Div5"><asp:CheckBox ID="CheckBox5" runat="server" Text="Fr" /></div>
            <div id ="Div6"><asp:CheckBox ID="CheckBox6" runat="server" Text="Sa" /></div>
            <div id ="Div7"><asp:CheckBox ID="CheckBox7" runat="server" Text="Su" /></div>
            </div>
            <asp:Label ID="Label9" runat="server" Text="Day of Week Parked"></asp:Label>
            <%--<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>--%>
            <div id ="DivSecond">
            <div id ="Div8"><asp:CheckBox ID="CheckBox8" runat="server" Text="Mo" /></div>
            <div id ="Div9"><asp:CheckBox ID="CheckBox9" runat="server" Text="Tu" /></div>
            <div id ="Div10"><asp:CheckBox ID="CheckBox10" runat="server" Text="We" /></div>
            <div id ="Div11"><asp:CheckBox ID="CheckBox11" runat="server" Text="Th" /></div>
            <div id ="Div12"><asp:CheckBox ID="CheckBox12" runat="server" Text="Fr" /></div>
            <div id ="Div13"><asp:CheckBox ID="CheckBox13" runat="server" Text="Sa" /></div>
            <div id ="Div14"><asp:CheckBox ID="CheckBox14" runat="server" Text="Su" /></div>
            </div>

            <div id ="DivCalculate">
            <asp:CheckBox ID="CheckBox15" runat="server" Text="Calculate rates by % off" AutoPostBack="True" OnCheckedChanged="CheckBox15_CheckedChanged" />
            <asp:TextBox ID="txtPerc" runat="server">0</asp:TextBox>
            <asp:Label ID="Label31" runat="server" Text="%"></asp:Label>
            <asp:Button ID="Button3" runat="server" Text="Calculate" OnClick="Button3_Click" />
            </div>

            <asp:Label ID="Label10" runat="server" Text="Min Stay Days"></asp:Label>
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <asp:Label ID="Label11" runat="server" Text="Max Stay Days"></asp:Label>
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>

            <asp:Label ID="Label12" runat="server" Text="TYPE"></asp:Label>
            <asp:Label ID="Label13" runat="server" Text="HOUR"></asp:Label>
            <asp:Label ID="Label14" runat="server" Text="DAYS"></asp:Label>
            <asp:Label ID="Label15" runat="server" Text="WEEK"></asp:Label>
            <asp:Label ID="Label16" runat="server" Text="MONTH"></asp:Label>
            <asp:Label ID="Label17" runat="server" Text="UPGRADE"></asp:Label>

            <asp:Label ID="Label18" runat="server" Text="RACK RATE"></asp:Label>
            <asp:Label ID="Label19" runat="server" Text="00.00"></asp:Label>
            <asp:Label ID="Label20" runat="server" Text="00.00"></asp:Label>
            <asp:Label ID="Label21" runat="server" Text="00.00"></asp:Label>
            <asp:Label ID="Label22" runat="server" Text="00.00"></asp:Label>
            <asp:Label ID="Label23" runat="server" Text="---"></asp:Label>

            <asp:Label ID="Label24" runat="server" Text="RATE"></asp:Label>
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:CheckBox ID="CheckBox16" runat="server" Text="" />
            <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <asp:CheckBox ID="CheckBox17" runat="server" Text="" />
            <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
            <asp:CheckBox ID="CheckBox18" runat="server" Text="" />
            <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
            <asp:CheckBox ID="CheckBox19" runat="server" Text="" />
            <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>

            <asp:Label ID="Label26" runat="server" Text="DISCOUNT (%)"></asp:Label>
            <asp:Label ID="Label27" runat="server" Text="00.00 (00%)"></asp:Label>
            <asp:Label ID="Label28" runat="server" Text="00.00 (00%)"></asp:Label>
            <asp:Label ID="Label29" runat="server" Text="00.00 (00%)"></asp:Label>
            <asp:Label ID="Label30" runat="server" Text="00.00 (00%)"></asp:Label>


            <asp:Label ID="Label25" runat="server" Text="SPECIAL NOTES"></asp:Label>
            <asp:TextBox ID="TextBox11" runat="server" TextMode="MultiLine"></asp:TextBox>


            <asp:Button ID="Button1" runat="server" Text="Generate Request" OnClick="Button1_Click" />

            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:HiddenField ID="HiddenField2" runat="server" />
            <asp:HiddenField ID="HiddenField3" runat="server" />
            <asp:HiddenField ID="HiddenField4" runat="server" />

        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server">

             <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"  Visible ="false"
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1005px" Height="751px" style="margin-right: 0px">
                <LocalReport ReportEmbeddedResource="pnfw1.Report1.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
            </rsweb:ReportViewer>

            <asp:Button ID="Button2" runat="server" Text="New Rates Request" OnClick="Button2_Click" />
            <asp:Button ID="Button6" runat="server" Text="Send To Approve" OnClick="Button6_Click" Visible="False" />

        </asp:Panel>
        


    </div>
    </form>
</body>
</html>
