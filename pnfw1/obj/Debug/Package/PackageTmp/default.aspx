﻿    <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="pnfw1.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StylesLogin.css" rel="stylesheet" />
    <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
</head>
<body>
    <form id="form1" runat="server">
    <div id ="BlockL">
        <asp:Label ID="Label1" runat="server" Text="Welcome to Park'N Fly"></asp:Label>
        <asp:Label ID="Label4" runat="server" Text="Sorry, you do not have rights for this application" Visible="false"> </asp:Label>
        <asp:Label ID="Label2" runat="server" Text="User"></asp:Label>
        <asp:Label ID="Label3" runat="server" Text="Password"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="OK" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Exit"  OnClientClick="window.close();" />

    </div>
    </form>
</body>
</html>
