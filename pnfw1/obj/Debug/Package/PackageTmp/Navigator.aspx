﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Navigator.aspx.cs" Inherits="pnfw1.Navigator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StylesNavigator.css" rel="stylesheet" />
    <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>

     <script type="text/javascript">
 
         $(function () {
             localStorage.setItem("refId", "0");
             localStorage.setItem("SC", "S");
             localStorage.setItem("CC", "N");
             localStorage.setItem("error", "N");
             localStorage.setItem("errorD", "");
        });
         </script>


</head>
<body>
    <form id="form1" runat="server">
    <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="NAVIGATOR"></asp:Label>

        <asp:Button ID="Button1" runat="server" Text="Reports" OnClick="Button1_Click" />
        <asp:Button ID="Button3" runat="server" Text="Exit" OnClick="Button3_Click" OnClientClick="javascript:window.close();" />
        
        <asp:Button ID="Button4" runat="server" Text="Charts" OnClick="Button4_Click" />
        <asp:Button ID="Button2" runat="server" Text="New Rates" OnClick="Button2_Click" />
        <asp:Button ID="Button5" runat="server" Text="Rates Switch" OnClick="Button5_Click" />
        <asp:Button ID="Button6" runat="server" Text="Jockey and Label Printers" OnClick="Button6_Click" />
        <asp:Button ID="Button7" runat="server" Text="Work with Referrals" OnClick="Button7_Click" />

    </div>
    </form>
</body>
</html>
