﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Charts.aspx.cs" Inherits="pnfw1.Charts" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="StylesCharts.css" rel="stylesheet" />
        <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
</head>
<body>
    <form id="form1" runat="server">
    <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="CHARTS"></asp:Label>
    <%--<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Run" />--%>

        <asp:Label ID="Label2" runat="server" Text="Measure"></asp:Label>
        <asp:Label ID="Label3" runat="server" Text="Month"></asp:Label>
        <asp:Label ID="Label4" runat="server" Text="City"></asp:Label>
        <asp:Label ID="Label5" runat="server" Text="Location"></asp:Label>



        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AutoPostBack="True">
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList3" runat="server" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged" AutoPostBack="True">
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList4" runat="server" OnSelectedIndexChanged="DropDownList4_SelectedIndexChanged" AutoPostBack="True">
        </asp:DropDownList>

        <br />

        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Navigator.aspx">Navigator</asp:HyperLink>

        <br />

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>       

        <br />



        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="1017px"  Height="750px">
        </rsweb:ReportViewer>



    </div>
    </form>
</body>
</html>
