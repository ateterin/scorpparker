﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JockeyLabel.aspx.cs" Inherits="pnfw1.JockeyLabel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="StylesJockeyLabel.css" rel="stylesheet" />
        <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
</head>
<body>
    <form id="form1" runat="server">
   <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="JOCKEY AND LABEL PRINTERS"></asp:Label>
        <asp:Label ID="Label4" runat="server" Text="Toronto Valet"></asp:Label>
 
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Navigator.aspx">Navigator</asp:HyperLink>

        <br />
       <asp:Button ID="Button1" runat="server" Text="Start Jockey Printer" OnClick="Button1_Click" />
       <asp:Button ID="Button2" runat="server" Text="Stop Jockey Printer" OnClick="Button2_Click" />
 
       <asp:Label ID="Label2" runat="server" Text="Jockey Printer"></asp:Label>

       <asp:Button ID="Button3" runat="server" Text="Start Label Printer" OnClick="Button3_Click" />
       <asp:Button ID="Button4" runat="server" Text="Stop Label Printer" OnClick="Button4_Click" />
 
       <asp:Label ID="Label3" runat="server" Text="Label Printer"></asp:Label>
 
    
       <asp:Label ID="Label5" runat="server" Text="* Please be patient: after button's click wait for 5 seconds"></asp:Label>
 
    
    </div>
    </form>
</body>
</html>