﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Threading;
using System.Configuration;

namespace pnfw1
{
    public static class GData
    {
        private static String strFileLock_ = String.Empty;
        public static string strUserName = "";

        public static void LogMessage(string strMessage)
        {
            string strLogFold = ConfigurationManager.AppSettings["LogFolder"].ToString();
            // stream writer to write in the log file
            StreamWriter objWriter = null;

            // get the current date and time
            DateTime dtNow = DateTime.Now;

            // get the time stamp
            String strTimeStamp = dtNow.ToString("yyyy-MM-dd HH:mm:ss.FFFF");

            StringBuilder strLoggedMessage = new StringBuilder();

            // construct the message to be logged
            // append the date and time stamp
            strLoggedMessage.Append(strTimeStamp);
            // append a blank space
            strLoggedMessage.Append(" ");
            // append the message
            strLoggedMessage.Append(strMessage);

            try
            {
                // enter the monitor to synchronize access to the file
                Monitor.Enter(strFileLock_);

                // create a file stream
                // changed the folder to log to
                FileStream objLogFile = new FileStream(string.Format(strLogFold + "\\Web_{0}.log", dtNow.ToString("yyyy_MM_dd")),
                                                       FileMode.OpenOrCreate,
                                                       FileAccess.Write);

                // move at the end of it
                objLogFile.Seek(0, SeekOrigin.End);

                // create a stream writer
                objWriter = new StreamWriter(objLogFile);

                // write the message that must be logged
                objWriter.WriteLine(strLoggedMessage.ToString());

                // flush the buffer
                objWriter.Flush();
            }
            catch (Exception)
            {
            }
            finally
            {
                // close the file if opened
                if (objWriter != null) objWriter.Close();

                // exit the monitor to allow other threads to access the log file
                Monitor.Exit(strFileLock_);
            }
        }
    }
}