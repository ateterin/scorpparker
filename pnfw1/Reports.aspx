﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="pnfw1._default" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
    <link href="Styles.css" rel="stylesheet" />
    <link href="calendar-blue.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-en.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=TextBox2.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%m/%d/%Y",
                //ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
        $(document).ready(function () {
            $("#<%=TextBox3.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%m/%d/%Y",
                //ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
        $(document).ready(function () {
            $("#<%=TextBox4.ClientID %>").dynDateTime({
                        showsTime: true,
                        ifFormat: "%m/%d/%Y",
                        //ifFormat: "%Y/%m/%d %H:%M",
                        daFormat: "%l;%M %p, %e %m,  %Y",
                        align: "BR",
                        electric: false,
                        singleClick: false,
                        displayArea: ".siblings('.dtcDisplayArea')",
                        button: ".next()"
                    });
                });
        $(document).ready(function () {
            $("#<%=TextBox5.ClientID %>").dynDateTime({
                        showsTime: true,
                        ifFormat: "%m/%d/%Y",
                        //ifFormat: "%Y/%m/%d %H:%M",
                        daFormat: "%l;%M %p, %e %m,  %Y",
                        align: "BR",
                        electric: false,
                        singleClick: false,
                        displayArea: ".siblings('.dtcDisplayArea')",
                        button: ".next()"
                    });
                });
    </script>
</head>
<body>
    <form id="form1" runat="server">

    <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="REPORTS"></asp:Label>
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Run" />

        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">

        </asp:DropDownList>

        <asp:DropDownList ID="DropDownList3" runat="server" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged" AutoPostBack="True">

        </asp:DropDownList>

        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Navigator.aspx">Navigator</asp:HyperLink>

        <br />

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>       

        <asp:Panel ID="Panel1" runat="server" Height="265px">
        <asp:Label ID="Label2" runat="server" Text="Location"></asp:Label>
        <asp:DropDownList ID="DropDownList2" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
            </asp:DropDownList>
        <asp:Label ID="Label3" runat="server" Text="Referral id"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server">3189</asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="txtRequiredValidator" ControlToValidate="TextBox1" ErrorMessage="Input should not be empty" />
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="TextBox1" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>

        <asp:Label ID="Label6" runat="server" Text="Start Date"></asp:Label>
        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
        <%--<img id="img3" src="calender.png" />--%>
            <asp:Image ID="img3" runat="server" ImageUrl="calender.png" />

        <asp:Label ID="Label7" runat="server" Text="Close Date"></asp:Label>
        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
        <%--<img id="img3" src="calender.png" />--%>
            <asp:Image ID="img4" runat="server" ImageUrl="calender.png" />


        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server" Height="265px">
        <asp:Label ID="Label4" runat="server" Text="Open Date"></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <img id="img1" src="calender.png" />
        <asp:Label ID="Label5" runat="server" Text="Close Date"></asp:Label>
        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        <%--<img id="img2" src="calender.png" />--%>
        <asp:Image ID="img2" runat="server" ImageUrl="calender.png" />
        </asp:Panel>

        <br />

        

        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"  Visible ="false" 
 
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1018px" Height="740px" style="margin-right: 0px">
            <LocalReport ReportEmbeddedResource="pnfw1.Report1.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>

 
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="PARKER_ALLDataSetTableAdapters.flip_ratesTableAdapter"></asp:ObjectDataSource>
    
    </div>
          


    </form>
</body>
</html>
