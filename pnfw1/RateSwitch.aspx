﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RateSwitch.aspx.cs" Inherits="pnfw1.RateSwitch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="StylesRateSwitch.css" rel="stylesheet" />
        <link rel="icon" href="/PNF3.ico" type="image/x-icon"/>
</head>
<body>
    <form id="form1" runat="server">
   <div id ="block1">
        <asp:Image ID="Image1" runat="server" Height="89px" ImageUrl="~/ParkNFly1.gif" Width="100px" />
        <asp:Label ID="Label1" runat="server" Text="RATE SWITCH"></asp:Label>
 

        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">

        </asp:DropDownList>
     

        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Navigator.aspx">Navigator</asp:HyperLink>

        <br />

    
        <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="Black" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="250px" NextPrevFormat="ShortMonth" Width="330px" OnDayRender="CalendarRenderer" OnSelectionChanged="Calendar1_SelectionChanged" OnVisibleMonthChanged="Calendar1_VisibleMonthChanged">
            <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="8pt" />
            <DayStyle BackColor="LightGreen" />
            <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
            <OtherMonthDayStyle ForeColor="#999999" />
            <SelectedDayStyle BackColor="DarkGreen" ForeColor="Black" />
            <TitleStyle BackColor="DarkGreen" BorderStyle="Solid" Font-Bold="True" Font-Size="12pt" ForeColor="White" Height="12pt" />
            <TodayDayStyle BackColor="LightGreen" ForeColor="Black" BorderStyle="Solid" BorderColor="Yellow" />
        </asp:Calendar>

    
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>

    
    </div>
    </form>
</body>
</html>
