﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace pnfw1
{
    public partial class Charts : System.Web.UI.Page
    {
        //dsLocations.locationsDataTable dtLocations = new dsLocations.locationsDataTable();
        //dsLocationsTableAdapters.locationsTableAdapter daLocations = new dsLocationsTableAdapters.locationsTableAdapter();

        dsRevenueSummary.dtRevenueSummaryDataTable dtRevSummary = new dsRevenueSummary.dtRevenueSummaryDataTable();
        dsRevenueSummaryTableAdapters.dtRevenueSummaryTableAdapter daRevSummary = new dsRevenueSummaryTableAdapters.dtRevenueSummaryTableAdapter();

        string loc = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["indA"].ToString() != "1")
            {

                string close = @"<script type='text/javascript'>
                                window.returnValue = true; 
                                window.close();
                                </script>";
                base.Response.Write(close);
            }

            if (!IsPostBack)
            {
                DropDownList1.Items.Add(new ListItem("Month To Date Revenue", "1"));
                DropDownList1.Items.Add(new ListItem("Daily Revenue", "2"));
                DropDownList1.Items.Add(new ListItem("Year To Date Revenue", "3"));
                DropDownList1.Items.IndexOf(DropDownList1.Items.FindByValue("1"));

                DropDownList2.Items.Add(new ListItem("January", "1"));
                DropDownList2.Items.Add(new ListItem("February", "2"));
                DropDownList2.Items.Add(new ListItem("March", "3"));
                DropDownList2.Items.Add(new ListItem("April", "4"));
                DropDownList2.Items.Add(new ListItem("May", "5"));
                DropDownList2.Items.Add(new ListItem("June", "6"));
                DropDownList2.Items.Add(new ListItem("July", "7"));
                DropDownList2.Items.Add(new ListItem("August", "8"));
                DropDownList2.Items.Add(new ListItem("September", "9"));
                DropDownList2.Items.Add(new ListItem("October", "10"));
                DropDownList2.Items.Add(new ListItem("November", "11"));
                DropDownList2.Items.Add(new ListItem("December", "12"));
                DropDownList2.SelectedIndex =
                DropDownList2.Items.IndexOf(DropDownList2.Items.FindByValue(DateTime.Now.Month.ToString()));

                loc = (string)Session["Locations"];
               

                switch (loc)
                {
                    case "100/101/102/103/104/105/106/107/108/113/129/130":
                        DropDownList3.Items.Add(new ListItem("ALL", "1"));
                        DropDownList3.Items.Add(new ListItem("Toronto", "2"));
                        DropDownList3.Items.Add(new ListItem("Edmonton", "3"));
                        DropDownList3.Items.Add(new ListItem("Vancouver", "4"));
                        DropDownList3.Items.Add(new ListItem("Montreal", "5"));
                        DropDownList3.Items.Add(new ListItem("Ottawa", "6"));
                        DropDownList4.Items.Clear();
                        DropDownList4.Items.Add(new ListItem("ALL", "100/101/102/103/104/105/106/107/108"));
                        DropDownList4.Visible = false;
                        Label5.Visible = false;
                        DrawChart("100/101/102/103/104/105/106/107/108", DropDownList2.SelectedValue.ToString(),DropDownList1.SelectedValue.ToString());
                        break;
                    case "100/101/102":
                        DropDownList3.Items.Add(new ListItem("Toronto", "2"));
                        DropDownList4.Items.Clear();
                        DropDownList4.Visible = true;;
                        Label5.Visible = true;
                        DropDownList4.Items.Add(new ListItem("ALL", "100/101/102"));
                        DropDownList4.Items.Add(new ListItem("Valet", "100"));
                        DropDownList4.Items.Add(new ListItem("SelfPark", "101"));
                        DropDownList4.Items.Add(new ListItem("Economy", "102"));
                        DrawChart("100/101/102", DropDownList2.SelectedValue.ToString(),DropDownList1.SelectedValue.ToString());
                        break;
                    case "103":
                        DropDownList3.Items.Add(new ListItem("Edmonton", "3"));
                        DropDownList4.Items.Clear();
                        DropDownList4.Visible = false;;
                        Label5.Visible = false;
                        DropDownList4.Items.Add(new ListItem("ALL", "103"));
                        DrawChart("103", DropDownList2.SelectedValue.ToString(),DropDownList1.SelectedValue.ToString());
                        break;
                    case "104":
                        DropDownList3.Items.Add(new ListItem("Vancouver", "4"));
                        DropDownList4.Items.Clear();
                        DropDownList4.Visible = false;;
                        Label5.Visible = false;
                        DropDownList4.Items.Add(new ListItem("ALL", "104"));
                        DrawChart("104", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                        break;
                    case "105/106/107":
                        DropDownList3.Items.Add(new ListItem("Montreal", "5"));
                        DropDownList4.Items.Clear();
                        DropDownList4.Visible = true;;
                        Label5.Visible = true;
                        DropDownList4.Items.Add(new ListItem("ALL", "105/106/107"));
                        DropDownList4.Items.Add(new ListItem("Valet", "105"));
                        DropDownList4.Items.Add(new ListItem("Express A", "106"));
                        DropDownList4.Items.Add(new ListItem("Express B", "107"));
                        DrawChart("105/106/107", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                        break;
                    case "108":
                        DropDownList3.Items.Add(new ListItem("Ottawa", "6"));
                        DropDownList4.Items.Clear();
                        DropDownList4.Visible = false;;
                        Label5.Visible = false;
                        DropDownList4.Items.Add(new ListItem("ALL", "108"));
                        DrawChart("108", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                        break;
                    default:
                        break;
                }
            }
        }

        protected void DrawChart(string strLocations, string strMonth, string iReport)
        {
            if (Convert.ToInt32(Session["Level"].ToString()) < 3)
            {
            daRevSummary.Fill(dtRevSummary, DropDownList3.SelectedItem.Text.ToString(), DropDownList4.SelectedItem.Text.ToString(), DateTime.Now.Year.ToString(), strMonth, strLocations);
            ReportViewer1.Visible = true;
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.Refresh();
            

            switch (iReport)
            {
                case "1":
                    ReportViewer1.LocalReport.ReportPath = @"ReportCharts.rdlc";
                    break;
                case "2":
                    ReportViewer1.LocalReport.ReportPath = @"ReportChartsRevDaily.rdlc";
                    break;
                case "3":
                    ReportViewer1.LocalReport.ReportPath = @"ReportChartsY.rdlc";
                    break;
                default:
                    break;
            }

            Microsoft.Reporting.WebForms.ReportDataSource dsRSummary = new Microsoft.Reporting.WebForms.ReportDataSource();
            dsRSummary.Value = dtRevSummary;
            dsRSummary.Name = "dsYearSummary";
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(dsRSummary);
            ReportViewer1.LocalReport.Refresh();
            }

        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{

        //}

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawChart(DropDownList4.SelectedValue.ToString(), DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strx = DropDownList2.SelectedValue.ToString();
            DrawChart(DropDownList4.SelectedValue.ToString(), DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (DropDownList3.SelectedValue.ToString())
            {
                case "1":
                    DropDownList4.Items.Clear();
                    DropDownList4.Items.Add(new ListItem("ALL", "100/101/102/103/104/105/106/107/108"));
                    DropDownList4.Visible = false;
                    Label5.Visible = false;
                    DrawChart("100/101/102/103/104/105/106/107/108", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                    break;
                case "2":
                    DropDownList4.Items.Clear();
                    DropDownList4.Visible = true;;
                    Label5.Visible = true;
                    DropDownList4.Items.Add(new ListItem("ALL", "100/101/102"));
                    DropDownList4.Items.Add(new ListItem("Valet", "100"));
                    DropDownList4.Items.Add(new ListItem("SelfPark", "101"));
                    DropDownList4.Items.Add(new ListItem("Economy", "102"));
                    DrawChart("100/101/102", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                    break;
                case "3":
                    DropDownList4.Items.Clear();
                    DropDownList4.Visible = false;;
                    Label5.Visible = false;
                    DropDownList4.Items.Add(new ListItem("ALL", "103"));
                    DrawChart("103", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                    break;
                case "4":
                    DropDownList4.Items.Clear();
                    DropDownList4.Visible = false;;
                    Label5.Visible = false;
                    DropDownList4.Items.Add(new ListItem("ALL", "104"));
                    DrawChart("104", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                    break;
                case "5":
                    DropDownList4.Items.Clear();
                    DropDownList4.Visible = true;;
                    Label5.Visible = true;
                    DropDownList4.Items.Add(new ListItem("ALL", "105/106/107"));
                    DropDownList4.Items.Add(new ListItem("Valet", "105"));
                    DropDownList4.Items.Add(new ListItem("Express A", "106"));
                    DropDownList4.Items.Add(new ListItem("Express B", "107"));
                    DrawChart("105/106/107", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                    break;
                case "6":
                    DropDownList4.Items.Clear();
                    DropDownList4.Visible = false;;
                    Label5.Visible = false;
                    DropDownList4.Items.Add(new ListItem("ALL", "108"));
                    DrawChart("108", DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
                    break;
                default:
                    DropDownList4.Items.Clear();
                    break;
            }
        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawChart(DropDownList4.SelectedValue.ToString(), DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString());
        }
    }
}